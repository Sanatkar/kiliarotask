//
//  KiliaroErrorDomain.swift
//  KiliaroApp
//
//  Created by Hooman Sanatkar on 1/13/22.
//

import Foundation

public struct KiliaroErrorDomain {
    private static let prefix: String = {
        return Bundle.main.bundleIdentifier! + ".error"
    }()
    
    static let generic = prefix
    static let network = prefix + ".network"
}
