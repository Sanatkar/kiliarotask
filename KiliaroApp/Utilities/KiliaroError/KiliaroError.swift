//
//  KiliaroError.swift
//  KiliaroApp
//
//  Created by Hooman Sanatkar on 1/13/22.
//

import Foundation

public class KiliaroError: Error, Decodable {
    // MARK: - Properties
    public private(set) var domain = KiliaroErrorDomain.generic
    public private(set) var code = -1
    public private(set) var message = ""
    
    // MARK: - Methods
    public required init(domain: String, code: Int = -1, message: String) {
        self.domain = domain
        self.code = code
        self.message = message
    }
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        code = try container.decode(Int.self, forKey: .status)
        let subConstainer = try container.nestedContainer(keyedBy: ErrorCodingKeys.self, forKey: .data)
        message = try subConstainer.decode(String.self, forKey: .message)
        
    }
    
    public func setDomain   (domain: String) {
        self.domain = domain
    }
    
    
    enum CodingKeys: String, CodingKey {
        case status
        case data
    }
    
    enum ErrorCodingKeys: String, CodingKey {
        case message
    }
    
    var isSNPError: Bool {
        return domain.hasPrefix(KiliaroErrorDomain.generic)
    }
    
    public static func generic() -> KiliaroError {
        let genericErrorMessage =  NSLocalizedString("Unknown error", comment: "Generic error")
        return KiliaroError(domain: KiliaroErrorDomain.generic, code: -1, message: genericErrorMessage)
    }
    
    public static func network() -> KiliaroError {
        let networkErrorMessage =  NSLocalizedString("The Internet connection appears to be offline.", comment: "The Internet connection appears to be offline.")
        return KiliaroError(domain: KiliaroErrorDomain.network, code: -1009, message: networkErrorMessage)
    }
}
