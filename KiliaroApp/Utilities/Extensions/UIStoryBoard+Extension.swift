//
//  UIStoryBoard+Extension.swift
//  KiliaroApp
//
//  Created by Hooman Sanatkar on 1/15/22.
//

import Foundation
import UIKit

public extension UIStoryboard {
    static func loadViewController<T>(name: String = String(describing: T.self)) -> T {
        return UIStoryboard(name: name, bundle: .main).instantiateInitialViewController() as! T
    }
}
