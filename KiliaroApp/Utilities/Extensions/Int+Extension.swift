//
//  Int+Extension.swift
//  KiliaroApp
//
//  Created by Hooman Sanatkar on 1/20/22.
//

import Foundation

extension Int {
    func sizePrettyPrint() -> String {
        var convertedValue: Double = Double(self)
        var multiplyFactor = 0
        let tokens = ["", "K", "M", "G", "T", "P",  "E",  "Z", "Y"]
        while convertedValue > 1024 {
            convertedValue /= 1024
            multiplyFactor += 1
        }
        return String(format: "%4.2f %@", convertedValue, tokens[multiplyFactor])
    }
}
