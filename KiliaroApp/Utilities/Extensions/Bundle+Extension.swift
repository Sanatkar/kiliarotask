//
//  Bundle+Extension.swift
//  KiliaroApp
//
//  Created by Hooman Sanatkar on 1/21/22.
//

import Foundation

public extension Bundle {
    func load<T>(_ nibName: String = String(describing: T.self), owner: Any? = nil) -> T {
        return loadNibNamed(nibName, owner: owner, options: nil)?.first as! T
    }
}
