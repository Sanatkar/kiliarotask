//
//  String+Extension.swift
//  KiliaroApp
//
//  Created by Hooman Sanatkar on 1/21/22.
//

import Foundation

extension String {
    public func formatDateToCurrentLocale() -> String {
        let dateFormatter = DateFormatter()
        let gregorianCalendar = Calendar(identifier: .gregorian)
        dateFormatter.calendar = gregorianCalendar
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        guard let date = dateFormatter.date(from: self) else {
            return ""
        }
        
        dateFormatter.dateFormat = "yyyy-MMMM-dd | HH:mm"
        var dateString = dateFormatter.string(from: date)
        dateString = dateString.replacingOccurrences(of: "-", with: " - ")
        let dateArray = dateString.components(separatedBy: " - ")
        dateString = dateArray[2] + " - " + dateArray[1] + " - " + dateArray [0]
        return dateString
    }
}
