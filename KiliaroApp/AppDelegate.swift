//
//  AppDelegate.swift
//  KiliaroApp
//
//  Created by Hooman Sanatkar on 1/11/22.
//

import UIKit
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var appCoordinator: AppCoordinator!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        if UserDefaultsManager().string(forKey: .sharedID) == nil {
            UserDefaultsManager().set("djlCbGusTJamg_ca4axEVw", forKey: .sharedID)
        }
        
        AppCoordinator.shared.start()
        window = UIWindow()
        window?.rootViewController = AppCoordinator.shared.navigationController
        window?.makeKeyAndVisible()
        IQKeyboardManager.shared.enable = true
        
        return true
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        print("will ennter forground")
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        print("did Enter Background")
    }


}

