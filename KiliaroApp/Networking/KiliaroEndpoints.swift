//
//  TwitterEndpoints.swift
//  KiliaroApp
//
//  Created by Hooman Sanatkar on 1/13/22.
//

import Foundation

public typealias Parameters = [String:Any]
public typealias Headers = [String:String]

enum KiliaroApi{
    case getPhotos(sharedKey: String)
}

extension KiliaroApi: EndPointSpecs{
    
    var scheme: String {
        return ServerManager.shared.scheme
    }
    
    var host: String{
        return ServerManager.shared.host
    }
    
    var path: String{
        switch self {
        case .getPhotos(let sharedKey):
            return "/shared/\(sharedKey)/media"
        // should be filled for upcoming endpoints
        }
    }
    
    var timeout: Double {
        return 10
    }
    
    var httpMethod: String {
        switch self {
        case .getPhotos:
            return "get"
        // should be filled for upcoming endpoints
        }
    }
    
    var queryParams: [URLQueryItem]? {
        switch self {
        case .getPhotos:
            return []
        }
    }
    
    var params: Parameters? {
        switch self {
        case .getPhotos:
            return nil
        // should be filled for upcoming endpoints
        }
        
    }
    
    var headers: Headers {
        var headers: Headers = [:]
        return headers
    }
}
