//
//  EndpointSpecs.swift
//  KiliaroApp
//
//  Created by Hooman Sanatkar on 1/13/22.
//

import Foundation

protocol EndPointSpecs {
    var scheme: String { get }
    var host: String { get }
    var path: String { get }
    var timeout: Double { get }
    var httpMethod: String { get }
    var queryParams: [URLQueryItem]? { get }
    var params: Parameters? { get }
    var headers: Headers { get }
}
