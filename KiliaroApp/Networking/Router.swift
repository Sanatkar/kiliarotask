//
//  Router.swift
//  KiliaroApp
//
//  Created by Hooman Sanatkar on 1/13/22.
//

import Foundation
import Alamofire

public typealias NetworkRouterCompletion = (_ data: Data?,_ error: Error?)->()

protocol NetworkRouter: class {
    associatedtype EndPoint: EndPointSpecs
    func request(_ route: EndPointSpecs, completion: @escaping NetworkRouterCompletion)
}

class Router<EndPoint: EndPointSpecs>: NetworkRouter {

   
    func request(_ route: EndPointSpecs, completion: @escaping NetworkRouterCompletion) {
        AF.request(getURL(from: route), method: HTTPMethod(rawValue: route.httpMethod), parameters: route.params, encoding: JSONEncoding.default, headers:  HTTPHeaders(route.headers))
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                switch response.result {
                case .success:
                    completion(response.data, nil)
                case .failure(let error):
                    completion(nil, error)
                }
            }
    }
    
    private func getURL(from route: EndPointSpecs) -> URL {
        var components = URLComponents()
        components.scheme = route.scheme
        components.host = route.host
        components.path = route.path
        if let queryItems = route.queryParams {
            components.queryItems = queryItems
        }
        return components.url!
    }
    
}
