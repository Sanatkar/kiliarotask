//
//  Photo.swift
//  KiliaroApp
//
//  Created by Hooman Sanatkar on 1/11/22.
//

import Foundation

enum MediaType: String, Codable, CaseIterableDefaultsLast {
    case video = "video"
    case photo = "image"
    case unknown
}

public struct Media: Codable {
    let id: String
    let userID: String?
    let mediaType: MediaType?
    let filename: String?
    let size: Int?
    let createdAt: String?
    let thumbnailURL: String?
    let downloadURL: String?
    let resx: Int?
    let resy: Int?
    
}

extension Media {
    enum CodingKeys: String, CodingKey {
        case id
        case userID = ""
        case mediaType = "media_type"
        case filename
        case size
        case createdAt = "created_at"
        case thumbnailURL = "thumbnail_url"
        case downloadURL = "download_url"
        case resx
        case resy
    }
}
