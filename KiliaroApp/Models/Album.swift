//
//  Album.swift
//  KiliaroApp
//
//  Created by Hooman Sanatkar on 1/11/22.
//

import Foundation

public struct Album: Codable {
    let id: String
    let userID: String?
    let name: String?
    let count: Int?
    let coverMediaID: String?
    let userShareID: String?
}

extension Album {
    enum CodingKeys: String, CodingKey {
        case id
        case userID = "user_id"
        case name = "name"
        case count
        case coverMediaID = "cover_media_id"
        case userShareID = "usershare_id"
    }
}
