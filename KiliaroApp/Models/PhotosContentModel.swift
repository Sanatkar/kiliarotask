//
//  PhotosResponse.swift
//  KiliaroApp
//
//  Created by Hooman Sanatkar on 1/20/22.
//

import Foundation

protocol PhotosContentModelProtocol: Codable {
    
}


class PhotosContentModel: PhotosContentModelProtocol {
    let photos: [Media]?
}
