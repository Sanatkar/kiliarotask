//
//  AlbumCoordinator.swift
//  KiliaroApp
//
//  Created by Hooman Sanatkar on 1/15/22.
//

import Foundation
import UIKit

class AlbumCoordinator {
    var navigationController: UINavigationController
    var networkMannager: NetworkManager

    init(navigationController: UINavigationController, networkManager: NetworkManager) {
        self.navigationController = navigationController
        self.networkMannager = networkManager
    }

    func start() {
        let (viewController, _) = PhotosConfigurator.build(networkManager: networkMannager)
        navigationController.pushViewController(viewController, animated: false)
    }
}
