//
//  AppCoordinator.swift
//  KiliaroApp
//
//  Created by Hooman Sanatkar on 1/15/22.
//

import Foundation
import UIKit


class AppCoordinator {
    
    // MARK: Properties
    
    var navigationController: UINavigationController
    static var shared = AppCoordinator()
    private let albumCoordinator: AlbumCoordinator
    private let networkManager = NetworkManager()
    
    // MARK: Initialization Methods
    
    private init() {
        // create the main navigation controller to be used for our app
        self.navigationController = UINavigationController()
        self.navigationController.navigationBar.shadowImage = UIImage()
        self.navigationController.navigationBar.isTranslucent = false
        self.navigationController.view.backgroundColor = UIColor.init(named: "AccentColor")
        albumCoordinator = AlbumCoordinator(navigationController: self.navigationController, networkManager: networkManager)
    }
    
    // MARK: Start Methods
    func start() {
        albumCoordinator.start()
    }
}
