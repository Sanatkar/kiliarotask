//
//  KiliaroSceneProtocols.swift
//  KiliaroApp
//
//  Created by Hooman Sanatkar on 1/15/22.
//

import Foundation

import UIKit

public protocol KiliaroSceneInteractorProtocol: class {
    func viewDidLoad()
    func viewWillAppear(_ animated: Bool)
    func viewDidAppear(_ animated: Bool)
    func viewWillDisappear(_ animated: Bool)
    func viewDidDisappear(_ animated: Bool)

}

//sourcery: AutoMockable
public protocol KiliaroScenePresenterProtocol: class {
    func handle(error: KiliaroError?)
}


//sourcery: AutoMockable
public protocol KiliaroSceneViewControllerProtocol: class {
    var viewController: KiliaroSceneViewController! { get }
    
}

public extension KiliaroSceneViewControllerProtocol where Self: KiliaroSceneViewController {
    var viewController: KiliaroSceneViewController! {
        return self
    }
}

public protocol KiliaroSceneRouterProtocol: class {
}



public extension KiliaroSceneInteractorProtocol {
    func viewDidLoad() {}
    func viewWillAppear(_ animated: Bool) {}
    func viewDidAppear(_ animated: Bool) {}
    func viewWillDisappear(_ animated: Bool) {}
    func viewDidDisappear(_ animated: Bool) {}

}
public extension KiliaroScenePresenterProtocol {
    func handle(error: KiliaroError?) {
        handle(error: error, dismiss: nil)
    }
    
    func handle(error: KiliaroError?, dismiss: (() -> Void)?) {}
}
