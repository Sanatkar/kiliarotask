//
//  SettingsViewController.swift
//  KiliaroApp
//
//  Created by Hooman Sanatkar on 1/13/22.
//

import Foundation
import UIKit

class SettingsViewController: KiliaroSceneViewController {
    // MARK: - Properties
    var settingsInteractor: SettingsInteractorProtocol! {
        get {
            return interactor as? SettingsInteractorProtocol
        }
        set {
            interactor = newValue
        }
    }
   
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var invalidateCacheButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    private func setupViews() {
        closeButton.layer.cornerRadius = 5
        closeButton.setTitle("Close", for: .normal)
        closeButton.addTarget(self, action: #selector(closePressed), for: .touchUpInside)
        
        invalidateCacheButton.layer.cornerRadius = 5
        invalidateCacheButton.setTitle("Invalidate Cache", for: .normal)
        invalidateCacheButton.addTarget(self, action: #selector(invalidateCachePressed), for: .touchUpInside)
    }
    
    @objc private func closePressed() {
        settingsInteractor.didTapOnClose()
    }
    
    @objc private func invalidateCachePressed() {
        settingsInteractor.didTapOnInvalidateCache()
    }

}

extension SettingsViewController: SettingsViewControllerProtocol {
    func showError(_ error: KiliaroError?) {
        
    }
}
