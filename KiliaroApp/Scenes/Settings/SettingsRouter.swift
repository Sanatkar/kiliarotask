//
//  SettingsRouter.swift
//  KiliaroApp
//
//  Created by Hooman Sanatkar on 1/13/22.
//

import Foundation

class SettingsRouter: KiliaroSceneRouter {
    // MARK: - Properties
    weak var viewControllerProtocol: SettingsViewControllerProtocol!
    // MARK: - Methods
}

extension SettingsRouter: SettingsRouterProtocol {
    func dismiss() {
        viewControllerProtocol.viewController.dismiss(animated: true)
    }
}
