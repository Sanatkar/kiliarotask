//
//  SettingsProtocols.swift
//  KiliaroApp
//
//  Created by Hooman Sanatkar on 1/13/22.
//

import Foundation

//sourcery: AutoMockable
protocol SettingsInteractorProtocol: KiliaroSceneInteractorProtocol {
    func didTapOnClose()
    func didTapOnInvalidateCache()
}

//sourcery: AutoMockable
protocol SettingsPresenterProtocol: KiliaroScenePresenterProtocol {
}

//sourcery: AutoMockable
protocol SettingsViewControllerProtocol: KiliaroSceneViewControllerProtocol {
    func showError(_ error: KiliaroError?)
}

//sourcery: AutoMockable
protocol SettingsRouterProtocol: KiliaroSceneRouterProtocol {
    func dismiss()
}
