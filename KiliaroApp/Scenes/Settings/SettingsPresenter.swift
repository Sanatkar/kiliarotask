//
//  SettingsPresenter.swift
//  KiliaroApp
//
//  Created by Hooman Sanatkar on 1/13/22.
//

import Foundation

class SettingsPresenter: KiliaroScenePresenter {
    // MARK: - Properties
    weak var viewController: SettingsViewControllerProtocol!
    
    // MARK: - Methods
}

extension SettingsPresenter: SettingsPresenterProtocol {
   
}
