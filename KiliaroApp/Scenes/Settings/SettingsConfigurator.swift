//
//  SettingsConfigurator.swift
//  KiliaroApp
//
//  Created by Hooman Sanatkar on 1/13/22.
//

import Foundation
import UIKit

class SettingsConfigurator: KiliaroSceneConfigurator {
    // MARK: - Properties
    
    // MARK: - Methods
    class func build() -> (SettingsViewController, SettingsInteractor) {
        let viewController: SettingsViewController = UIStoryboard.loadViewController()
        let interactor = SettingsInteractor()
        let presenter = SettingsPresenter()
        let router = SettingsRouter()
        
        viewController.interactor = interactor
        
        interactor.viewController = viewController
        interactor.presenter = presenter
        interactor.router = router
        
        presenter.viewController = viewController
        
        router.viewControllerProtocol = viewController
        
        return (viewController, interactor)
    }
}
