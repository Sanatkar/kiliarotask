//
//  SettingsInteractor.swift
//  KiliaroApp
//
//  Created by Hooman Sanatkar on 1/13/22.
//

import Foundation

class SettingsInteractor: KiliaroSceneInteractor {
    // MARK: - Properties
    var presenter: SettingsPresenterProtocol!
    var router: SettingsRouterProtocol!
    weak var viewController: SettingsViewControllerProtocol!
    
    func viewDidLoad() {
       
    }
}

extension SettingsInteractor: SettingsInteractorProtocol {
    func didTapOnClose() {
        router.dismiss()
    }
    
    func didTapOnInvalidateCache() {
        UserDefaultsManager().sharedUserDefaults?.removeObject(forKey: UserDefaultsKeys.savedPhotosData.rawValue)
    }
}
