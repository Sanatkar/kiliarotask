//
//  StreanerViewController.swift
//  KiliaroApp
//
//  Created by Hooman Sanatkar on 1/13/22.
//

import Foundation
import UIKit
import MapKit

class PhotosViewController: KiliaroSceneViewController {
    
    private struct Images {
        
    }
    
    private struct Colors {
        static let titleBackColor = UIColor.init(named: "AccentColor")
        static let frontColor = UIColor.white
    }
    
    struct Constants {
        static let cellIdentifier = "PhotoCell"
    }
    
    var activityIndicator: UIActivityIndicatorView!
    var collectionView: UICollectionView!
    
    var photos: [PhotoViewModel] = [] {
        didSet {
            collectionView.reloadData()
        }
    }
    
    // MARK: - Properties
    var photosInteractor: PhotosInteractorProtocol! {
        get {
            return interactor as? PhotosInteractorProtocol
        }
        set {
            interactor = newValue
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupNavigationBar()
        photosInteractor.fetchAlbumPhotos()
    }
    
    func setupViews() {
        activityIndicator = UIActivityIndicatorView()
        activityIndicator.hidesWhenStopped = true
        activityIndicator.stopAnimating()
        activityIndicator.color = .white
        activityIndicator.style = .medium
        configCollectionView()
    }
    
    func configCollectionView() {
        collectionView = UICollectionView(frame: view.frame, collectionViewLayout: UICollectionViewFlowLayout())
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        view.addExpletiveSubView(view: collectionView)
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        collectionView.register(UINib(nibName: "PhotoCell", bundle: .main), forCellWithReuseIdentifier: Constants.cellIdentifier)
    }
    
    func setupNavigationBar() {
        self.title = "Kiliaro"
        
        let settingButton = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action:  #selector(settingsTapped))
        settingButton.tintColor = .white
        self.navigationItem.rightBarButtonItem  = settingButton
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(customView: activityIndicator)
    }
    
    @objc func settingsTapped() {
        photosInteractor.tapOnSettings()
    }
    
    @objc private func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        self.navigationController?.isNavigationBarHidden = false
        sender.view?.removeFromSuperview()
    }
    
}

extension PhotosViewController: PhotosViewControllerProtocol {
    
    func showError(_ error: KiliaroError?) {
        let alert = UIAlertController(title: "Error", message: error?.message ?? "Something happened wrong", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showStartLoading() {
        activityIndicator.startAnimating()
    }
    
    func showStopLoading() {
        activityIndicator.stopAnimating()
    }
    
    func showPhotos(photos: [PhotoViewModel]) {
        self.photos = photos
    }
}

extension PhotosViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.cellIdentifier, for: indexPath) as! PhotoCell
        cell.fillData(photoViewModel: photos[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemSize = (collectionView.frame.size.width - 30) / 3.0
        return CGSize(width: itemSize, height: itemSize)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let photoItem = photos[indexPath.row]
        guard let downloadURL = photoItem.downloadURL else {
            return
        }
        let photosDetilView = PhotoDetailView()
        photosDetilView.frame = UIScreen.main.bounds
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        photosDetilView.addGestureRecognizer(tap)
        self.view.addSubview(photosDetilView)
        self.navigationController?.isNavigationBarHidden = true
        photosDetilView.loadImage(imageURL: downloadURL, createdAt: photoItem.createdAt ?? "")
    }
}
