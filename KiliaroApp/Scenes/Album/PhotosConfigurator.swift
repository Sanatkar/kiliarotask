//
//  PhotosConfigurator.swift
//  KiliaroApp
//
//  Created by Hooman Sanatkar on 1/13/22.
//

import Foundation
import UIKit

class PhotosConfigurator: KiliaroSceneConfigurator {
    // MARK: - Properties
    
    // MARK: - Methods
    class func build(networkManager: NetworkManager) -> (PhotosViewController, PhotosInteractor) {
        let viewController: PhotosViewController = UIStoryboard.loadViewController() 
        let interactor = PhotosInteractor()
        let presenter = PhotosPresenter()
        let router = PhotosRouter()
        
        viewController.interactor = interactor
        
        interactor.viewController = viewController
        interactor.presenter = presenter
        interactor.router = router
        interactor.networkManager = networkManager
        
        presenter.viewController = viewController
        
        router.viewControllerProtocol = viewController
        
        return (viewController, interactor)
    }
}
