//
//  PhotoViewModel.swift
//  KiliaroApp
//
//  Created by Hooman Sanatkar on 1/14/22.
//

import Foundation

enum ResizingMode {
    case boundingBox
    case crop
    case minimumDimension
}


public struct PhotoViewModel {
    let id: String
    let size: Int?
    let createdAt: String?
    let thumbnailURL: String?
    let downloadURL: String?
    let resx: Int?
    let resy: Int?
}

extension PhotoViewModel {
    func optimizedThumbnailURL(width: Int, height: Int, resizingMode: ResizingMode) -> URL? {
        guard let thumbnailURL = thumbnailURL else {
            return nil
        }
        var resizingModeParam = ""
        switch resizingMode {
        case .boundingBox:
            resizingModeParam = "bb"
            break
        case .crop:
            resizingModeParam = "crop"
            break
        case .minimumDimension:
            resizingModeParam = "md"
            break
        }
        let queryItems = [URLQueryItem(name: "w", value: "\(width)"), URLQueryItem(name: "h", value: "\(height)"), URLQueryItem(name: "mode", value: "\(resizingModeParam)")]
        var urlComps = URLComponents(string: thumbnailURL)!
        urlComps.queryItems = queryItems
        return urlComps.url
    }
        
}
