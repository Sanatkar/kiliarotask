//
//  PhotosInteractor.swift
//  KiliaroApp
//
//  Created by Hooman Sanatkar on 1/13/22.
//

import Foundation

class PhotosInteractor: KiliaroSceneInteractor {
    // MARK: - Properties
    var presenter: PhotosPresenterProtocol!
    var router: PhotosRouterProtocol!
    var photos: [Media] = []
    var photosContentManager: PhotosContentManager?
    weak var viewController: PhotosViewControllerProtocol!
    
    public var networkManager: NetworkManager!
    
    func viewDidLoad() {
        photosContentManager = PhotosContentManager(networkManager: self.networkManager)
    }
    
}

extension PhotosInteractor: PhotosInteractorProtocol {
    
    func fetchAlbumPhotos() {
        photosContentManager?.requestData(completion: {[weak self] photos , error in
            if error != nil {
                self?.presenter.handle(error: error)
                return
            } else {
                // we are sure that photos has data here, so we can unwrap
                self?.presenter.presentPhotos(photos: photos!)
            }
        })
    }
    
    func tapOnSettings() {
        router.navigateToSettings()
    }
  
}
