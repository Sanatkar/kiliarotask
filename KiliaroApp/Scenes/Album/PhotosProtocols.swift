//
//  PhotosProtocols.swift
//  KiliaroApp
//
//  Created by Hooman Sanatkar on 1/13/22.
//

import Foundation

//sourcery: AutoMockable
protocol PhotosInteractorProtocol: KiliaroSceneInteractorProtocol {
    func fetchAlbumPhotos()
    func tapOnSettings()
}

//sourcery: AutoMockable
protocol PhotosPresenterProtocol: KiliaroScenePresenterProtocol {
    func presentStartLoading()
    func presentStopLoading()
    func presentPhotos(photos: [Media])
}

//sourcery: AutoMockable
protocol PhotosViewControllerProtocol: KiliaroSceneViewControllerProtocol {
    func showError(_ error: KiliaroError?)
    func showStartLoading()
    func showStopLoading()
    func showPhotos(photos: [PhotoViewModel])
}

//sourcery: AutoMockable
protocol PhotosRouterProtocol: KiliaroSceneRouterProtocol {
    func navigateToSettings()
    func dismiss()
}
