//
//  PhotosRouter.swift
//  KiliaroApp
//
//  Created by Hooman Sanatkar on 1/13/22.
//

import Foundation

class PhotosRouter: KiliaroSceneRouter {
    // MARK: - Properties
    weak var viewControllerProtocol: PhotosViewControllerProtocol!
    // MARK: - Methods
}

extension PhotosRouter: PhotosRouterProtocol {
    
    func navigateToSettings() {
        let (viewController, _) = SettingsConfigurator.build()
        viewControllerProtocol.viewController.present(viewController, animated: true, completion: nil)
    }
    
    func dismiss() {
        viewControllerProtocol.viewController.dismiss(animated: false)
    }
}
