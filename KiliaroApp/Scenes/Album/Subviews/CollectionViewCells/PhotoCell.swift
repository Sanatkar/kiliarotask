//
//  PhotoCell.swift
//  KiliaroApp
//
//   Created by Hooman Sanatkar on 1/13/22.
//

import UIKit
import SDWebImage

class PhotoCell: UICollectionViewCell {
    
    // MARK: - IBOutlet
    
    @IBOutlet var sizeLabel: UILabel!
    @IBOutlet var imageView: UIImageView!
    
    
    // MARK: - Constants
    
    class var identifier: String {
        "PhotoCell"
    }
        
    // MARK: - Properties
    
    // MARK: - Methods
    // MARK: NSObject
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = .white
        self.contentView.backgroundColor = .white
        
        sizeLabel.numberOfLines = 0
        sizeLabel.textAlignment = .center
        sizeLabel.lineBreakMode = .byTruncatingTail
        sizeLabel.text = ""
        
        imageView.contentMode = .scaleAspectFill
        imageView.backgroundColor = .lightGray
        imageView.clipsToBounds = true
    }
    
    func fillData(photoViewModel: PhotoViewModel) {
        sizeLabel.text = (photoViewModel.size ?? 0).sizePrettyPrint() + "B"
        if let url = photoViewModel.optimizedThumbnailURL(width: Int(imageView.frame.size.width * 3), height: Int(imageView.frame.size.height * 3), resizingMode: .boundingBox) {
            imageView.image = nil
            imageView.sd_setImage(with: url, completed: nil)
        } else {
            imageView.image = nil
        }
    }
}
