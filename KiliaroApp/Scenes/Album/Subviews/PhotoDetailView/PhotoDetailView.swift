//
//  PhotoDetailView.swift
//  KiliaroApp
//
//  Created by Hooman Snatkar on 1/21/21.
//

import UIKit

class PhotoDetailView: UIView {
    // MARK: IBOutlets
    
    private weak var view: UIView!
    @IBOutlet var label: UILabel!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    // MARK: Inits Methods
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView() {
        let view: UIView = Bundle.main.load("PhotoDetailView", owner: self)
        addExpletiveSubView(view: view)
        self.view = view
        self.backgroundColor = .black
        self.view.backgroundColor = .black
        
        setupSubviews()
    }
    
    private func setupSubviews() {
      
        label.textAlignment = .center
        label.textColor = .white
        label.text = ""
        
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFit
        
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
        activityIndicator.style = .large
        activityIndicator.color = .white
    }
    
    func loadImage(imageURL: String, createdAt: String) {
        label.text = createdAt
        imageView.downloaded(from: imageURL, completion: {[weak self] success in
            self?.activityIndicator.stopAnimating()
            if !success {
                let alert = UIAlertController(title: "Error", message: "can not load photo", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                self?.superview?.findViewController()!.present(alert, animated: true, completion: nil)
            }
        })
    }
}
