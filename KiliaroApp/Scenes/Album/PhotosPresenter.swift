//
//  PhotosPresenter.swift
//  KiliaroApp
//
//  Created by Hooman Sanatkar on 1/13/22.
//

import Foundation

class PhotosPresenter: KiliaroScenePresenter {
    // MARK: - Properties
    weak var viewController: PhotosViewControllerProtocol!
    
    public func getPhotoViewModel(photo: Media) -> PhotoViewModel {
        return PhotoViewModel.init(id: photo.id, size: photo.size, createdAt: (photo.createdAt ?? "").formatDateToCurrentLocale() , thumbnailURL: photo.thumbnailURL, downloadURL: photo.downloadURL, resx: photo.resx, resy: photo.resy)
    }
    
    // MARK: - Methods
}

extension PhotosPresenter: PhotosPresenterProtocol {
    
    
    func presentStartLoading() {
        viewController.showStartLoading()
    }
    func presentStopLoading() {
        viewController.showStopLoading()
    }
    func presentPhotos(photos: [Media]) {
        viewController.showPhotos(photos: photos.compactMap({
            return getPhotoViewModel(photo: $0)
        }))
    }
    func handle(error: KiliaroError?) {
        viewController.showError(error)
    }
}
