//
//  PhotosContentManager.swift
//  KiliaroApp
//
//  Created by Hooman Sanatkar on 1/20/22.
//

import Foundation

//sourcery: AutoMockable
protocol PhotosContentManagerProtocol {
    func requestData(completion: @escaping ([Media]?, KiliaroError?) -> Void)
}

class PhotosContentManager: PhotosContentManagerProtocol {
    var networkManager: NetworkManager
    init (networkManager: NetworkManager) {
        self.networkManager = networkManager
    }
    
    private func getCachedData<T: Codable>() -> T? {
        guard let model: T = UserDefaultsManager().codable(forKey: .savedPhotosData) else {
            return nil
        }
        return model
    }
    
    func requestData(completion: @escaping ([Media]?, KiliaroError?) -> Void) {
        if Connectivity.isConnectedToInternet {
            self.networkManager.getPhotos(sharedKey: UserDefaultsManager().string(forKey: .sharedID)!, completion: { photos, error in
                if error != nil {
                   completion(nil, error)
                } else {
                    UserDefaultsManager().set(codable: photos, forKey: .savedPhotosData)
                    completion(photos, nil)
                }
            })
         } else {
             guard let model: [Media] = getCachedData() else {
                 completion(nil, KiliaroError.init(domain: "", message: "There is no cached data, and network is offline"))
                 return
             }
           
            completion(model, nil)
        }
    }
}
