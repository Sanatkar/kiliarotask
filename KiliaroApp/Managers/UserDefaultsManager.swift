//
//  UserDefaultsManager.swift
//  KiliaroApp
//
//  Created by Hooman Sanatkar on 1/13/22.
//

import Foundation


enum UserDefaultsKeys: String {
    case sharedID = "share_id"
    case savedPhotosData = "saved_photos_data"
}

protocol UserDefaultsManagerProtocol {
    // MARK: Set Methods
    func set(_ value: String, forKey key: UserDefaultsKeys)
    func set(_ value: Bool, forKey key: UserDefaultsKeys)
    func set(_ value: Int, forKey key: UserDefaultsKeys)
    func set<T: Codable>(codable: T, forKey key: UserDefaultsKeys)

    // MARK: Get Methods
    func string(forKey key: UserDefaultsKeys) -> String?
    func codable<T: Codable>(forKey key: UserDefaultsKeys) -> T?
    func bool(forKey key: UserDefaultsKeys) -> Bool
    func optionalBool(forKey key: UserDefaultsKeys) -> Bool?
    func integer(forKey key: UserDefaultsKeys) -> Int

    // MARK: Custom Methods
    func clear(forKey key: UserDefaultsKeys)
    func clear()
    func synchronize()
}

class UserDefaultsManager: UserDefaultsManagerProtocol {

    let sharedUserDefaults: UserDefaults?

    init() {
        sharedUserDefaults = UserDefaults.standard
    }
    init(suiteName: String) {
        sharedUserDefaults = UserDefaults(suiteName: suiteName)
    }

    // MARK: Set Methods
    func set(_ value: String, forKey key: UserDefaultsKeys) {
        set(value: value as Any, forKey: key)
    }

    func set(_ value: Bool, forKey key: UserDefaultsKeys) {
        set(value: NSNumber(value: value), forKey: key)
    }

    func set(_ value: Int, forKey key: UserDefaultsKeys) {
        set(value: NSNumber(value: value), forKey: key)
    }

    private func set(value: Any?, forKey key: UserDefaultsKeys) {
        sharedUserDefaults?.set(value, forKey: key.rawValue)
        sharedUserDefaults?.synchronize()
    }

    func set<T: Codable>(codable: T, forKey key: UserDefaultsKeys) {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(codable) {
            let defaults = sharedUserDefaults
            defaults?.set(encoded, forKey: key.rawValue)
        }
    }

    // MARK: Get Methods
    func codable<T: Codable>(forKey key: UserDefaultsKeys) -> T? {
        if let saved = sharedUserDefaults?.object(forKey: key.rawValue) as? Data {
            let decoder = JSONDecoder()
            do {
             let codable = try decoder.decode(T.self, from: saved)
                return codable
            } catch {
//                log.error(error.localizedDescription)
                print(error.localizedDescription)
            }
        }
        return nil
    }

    func string(forKey key: UserDefaultsKeys) -> String? {
        let myVal: String? = value(forKey: key) as? String
        return myVal
    }

    func bool(forKey key: UserDefaultsKeys) -> Bool {
        if let value = value(forKey: key) as? Bool {
            return value
        }
        return false
    }
    func optionalBool(forKey key: UserDefaultsKeys) -> Bool? {
        let myVal: Bool? = value(forKey: key) as? Bool
        return myVal
    }
    func valueExists(forKey key: UserDefaultsKeys) -> Bool {
        if value(forKey: key) != nil {
            return true
        }
        return false
    }

    func integer(forKey key: UserDefaultsKeys) -> Int {
        if let value = value(forKey: key) as? Int {
            return value
        }
        return 0
    }

    private func value(forKey key: UserDefaultsKeys) -> Any? {
        return sharedUserDefaults?.value(forKey: key.rawValue)
    }

    // MARK: Clear
    func clear(forKey key: UserDefaultsKeys) {
        set(value: nil, forKey: key)
    }

    func clear() {
        // All keys except appleLanguages & currentLanguage
        let keys: [UserDefaultsKeys] = []
        for key in keys {
            sharedUserDefaults?.removeObject(forKey: key.rawValue)
        }
        synchronize()
    }

    func synchronize() {
        sharedUserDefaults?.synchronize()
        UserDefaults.standard.synchronize()
    }
}
