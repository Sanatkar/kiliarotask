//
//  NetworkManager.swift
//  KiliaroApp
//
//  Created by Hooman Sanatkar on 1/13/22.
//

import Foundation

enum NetworkResponse:String {
    case success
    case checkInternetConnection = "Plase check your internet connection"
    case authenticationError = "You need to be authenticated first."
    case badRequest = "Bad request"
    case outdated = "The url you requested is outdated."
    case failed = "Network request failed."
    case noData = "Response returned with no data to decode."
    case unableToDecode = "We could not decode the response."
}

enum Result<String>{
    case success
    case failure(String)
}

struct NetworkManager {
    let router = Router<KiliaroApi>()
        
    func getPhotos(sharedKey: String, completion: @escaping (_ phtos: [Media]?, _ error: KiliaroError?)->()){
        router.request(KiliaroApi.getPhotos(sharedKey: sharedKey)) { data, error in
            if error != nil {
                completion(nil, KiliaroError.init(domain: "", message: NetworkResponse.checkInternetConnection.rawValue))
            }
            guard let responseData = data else {
                completion(nil, KiliaroError.init(domain: "", message: NetworkResponse.noData.rawValue))
                return
            }
            do {
                if let photos = try JSONDecoder().decode([Media]?.self, from: responseData) {
                    completion(photos, nil)
                } else {
                    completion(nil, KiliaroError.init(domain: "", message: NetworkResponse.failed.rawValue))
                }
            } catch {
                completion(nil, KiliaroError.init(domain: "", message: NetworkResponse.failed.rawValue))
            }
        }
    }
    
    
}

