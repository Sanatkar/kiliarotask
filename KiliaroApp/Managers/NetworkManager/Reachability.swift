//
//  Reachability.swift
//  KiliaroApp
//
//  Created by Hooman Sanatkar on 1/20/22.
//

import Foundation
import Alamofire

struct Connectivity {
  static let sharedInstance = NetworkReachabilityManager()!
  static var isConnectedToInternet:Bool {
      return self.sharedInstance.isReachable
    }
}
