//
//  ServerManager.swift
//  KiliaroApp
//
//  Created by Hooman Sanatkar on 1/13/22.
//

import Foundation

class ServerManager {
   
    // MARK: - Properties
    static var shared = ServerManager()
    public let scheme = "https"
    public let host = "api1.kiliaro.com"
    
}
