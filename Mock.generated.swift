// Generated using Sourcery 1.6.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT


// Generated with SwiftyMocky 4.1.0
// Required Sourcery: 1.6.0


import SwiftyMocky
import XCTest
import Foundation
import UIKit
@testable import KiliaroApp


// MARK: - KiliaroScenePresenterProtocol

open class KiliaroScenePresenterProtocolMock: KiliaroScenePresenterProtocol, Mock {
    public init(sequencing sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst, stubbing stubbingPolicy: StubbingPolicy = .wrap, file: StaticString = #file, line: UInt = #line) {
        SwiftyMockyTestObserver.setup()
        self.sequencingPolicy = sequencingPolicy
        self.stubbingPolicy = stubbingPolicy
        self.file = file
        self.line = line
    }

    var matcher: Matcher = Matcher.default
    var stubbingPolicy: StubbingPolicy = .wrap
    var sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst

    private var queue = DispatchQueue(label: "com.swiftymocky.invocations", qos: .userInteractive)
    private var invocations: [MethodType] = []
    private var methodReturnValues: [Given] = []
    private var methodPerformValues: [Perform] = []
    private var file: StaticString?
    private var line: UInt?

    public typealias PropertyStub = Given
    public typealias MethodStub = Given
    public typealias SubscriptStub = Given

    /// Convenience method - call setupMock() to extend debug information when failure occurs
    public func setupMock(file: StaticString = #file, line: UInt = #line) {
        self.file = file
        self.line = line
    }

    /// Clear mock internals. You can specify what to reset (invocations aka verify, givens or performs) or leave it empty to clear all mock internals
    public func resetMock(_ scopes: MockScope...) {
        let scopes: [MockScope] = scopes.isEmpty ? [.invocation, .given, .perform] : scopes
        if scopes.contains(.invocation) { invocations = [] }
        if scopes.contains(.given) { methodReturnValues = [] }
        if scopes.contains(.perform) { methodPerformValues = [] }
    }





    open func handle(error: KiliaroError?) {
        addInvocation(.m_handle__error_error(Parameter<KiliaroError?>.value(`error`)))
		let perform = methodPerformValue(.m_handle__error_error(Parameter<KiliaroError?>.value(`error`))) as? (KiliaroError?) -> Void
		perform?(`error`)
    }

    open func handle(error: KiliaroError?, dismiss: (() -> Void)?) {
        addInvocation(.m_handle__error_errordismiss_dismiss(Parameter<KiliaroError?>.value(`error`), Parameter<(() -> Void)?>.value(`dismiss`)))
		let perform = methodPerformValue(.m_handle__error_errordismiss_dismiss(Parameter<KiliaroError?>.value(`error`), Parameter<(() -> Void)?>.value(`dismiss`))) as? (KiliaroError?, (() -> Void)?) -> Void
		perform?(`error`, `dismiss`)
    }


    fileprivate enum MethodType {
        case m_handle__error_error(Parameter<KiliaroError?>)
        case m_handle__error_errordismiss_dismiss(Parameter<KiliaroError?>, Parameter<(() -> Void)?>)

        static func compareParameters(lhs: MethodType, rhs: MethodType, matcher: Matcher) -> Matcher.ComparisonResult {
            switch (lhs, rhs) {
            case (.m_handle__error_error(let lhsError), .m_handle__error_error(let rhsError)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsError, rhs: rhsError, with: matcher), lhsError, rhsError, "error"))
				return Matcher.ComparisonResult(results)

            case (.m_handle__error_errordismiss_dismiss(let lhsError, let lhsDismiss), .m_handle__error_errordismiss_dismiss(let rhsError, let rhsDismiss)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsError, rhs: rhsError, with: matcher), lhsError, rhsError, "error"))
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsDismiss, rhs: rhsDismiss, with: matcher), lhsDismiss, rhsDismiss, "dismiss"))
				return Matcher.ComparisonResult(results)
            default: return .none
            }
        }

        func intValue() -> Int {
            switch self {
            case let .m_handle__error_error(p0): return p0.intValue
            case let .m_handle__error_errordismiss_dismiss(p0, p1): return p0.intValue + p1.intValue
            }
        }
        func assertionName() -> String {
            switch self {
            case .m_handle__error_error: return ".handle(error:)"
            case .m_handle__error_errordismiss_dismiss: return ".handle(error:dismiss:)"
            }
        }
    }

    open class Given: StubbedMethod {
        fileprivate var method: MethodType

        private init(method: MethodType, products: [StubProduct]) {
            self.method = method
            super.init(products)
        }


    }

    public struct Verify {
        fileprivate var method: MethodType

        public static func handle(error: Parameter<KiliaroError?>) -> Verify { return Verify(method: .m_handle__error_error(`error`))}
        public static func handle(error: Parameter<KiliaroError?>, dismiss: Parameter<(() -> Void)?>) -> Verify { return Verify(method: .m_handle__error_errordismiss_dismiss(`error`, `dismiss`))}
    }

    public struct Perform {
        fileprivate var method: MethodType
        var performs: Any

        public static func handle(error: Parameter<KiliaroError?>, perform: @escaping (KiliaroError?) -> Void) -> Perform {
            return Perform(method: .m_handle__error_error(`error`), performs: perform)
        }
        public static func handle(error: Parameter<KiliaroError?>, dismiss: Parameter<(() -> Void)?>, perform: @escaping (KiliaroError?, (() -> Void)?) -> Void) -> Perform {
            return Perform(method: .m_handle__error_errordismiss_dismiss(`error`, `dismiss`), performs: perform)
        }
    }

    public func given(_ method: Given) {
        methodReturnValues.append(method)
    }

    public func perform(_ method: Perform) {
        methodPerformValues.append(method)
        methodPerformValues.sort { $0.method.intValue() < $1.method.intValue() }
    }

    public func verify(_ method: Verify, count: Count = Count.moreOrEqual(to: 1), file: StaticString = #file, line: UInt = #line) {
        let fullMatches = matchingCalls(method, file: file, line: line)
        let success = count.matches(fullMatches)
        let assertionName = method.method.assertionName()
        let feedback: String = {
            guard !success else { return "" }
            return Utils.closestCallsMessage(
                for: self.invocations.map { invocation in
                    matcher.set(file: file, line: line)
                    defer { matcher.clearFileAndLine() }
                    return MethodType.compareParameters(lhs: invocation, rhs: method.method, matcher: matcher)
                },
                name: assertionName
            )
        }()
        MockyAssert(success, "Expected: \(count) invocations of `\(assertionName)`, but was: \(fullMatches).\(feedback)", file: file, line: line)
    }

    private func addInvocation(_ call: MethodType) {
        self.queue.sync { invocations.append(call) }
    }
    private func methodReturnValue(_ method: MethodType) throws -> StubProduct {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let candidates = sequencingPolicy.sorted(methodReturnValues, by: { $0.method.intValue() > $1.method.intValue() })
        let matched = candidates.first(where: { $0.isValid && MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch })
        guard let product = matched?.getProduct(policy: self.stubbingPolicy) else { throw MockError.notStubed }
        return product
    }
    private func methodPerformValue(_ method: MethodType) -> Any? {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let matched = methodPerformValues.reversed().first { MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch }
        return matched?.performs
    }
    private func matchingCalls(_ method: MethodType, file: StaticString?, line: UInt?) -> [MethodType] {
        matcher.set(file: file ?? self.file, line: line ?? self.line)
        defer { matcher.clearFileAndLine() }
        return invocations.filter { MethodType.compareParameters(lhs: $0, rhs: method, matcher: matcher).isFullMatch }
    }
    private func matchingCalls(_ method: Verify, file: StaticString?, line: UInt?) -> Int {
        return matchingCalls(method.method, file: file, line: line).count
    }
    private func givenGetterValue<T>(_ method: MethodType, _ message: String) -> T {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            onFatalFailure(message)
            Failure(message)
        }
    }
    private func optionalGivenGetterValue<T>(_ method: MethodType, _ message: String) -> T? {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            return nil
        }
    }
    private func onFatalFailure(_ message: String) {
        guard let file = self.file, let line = self.line else { return } // Let if fail if cannot handle gratefully
        SwiftyMockyTestObserver.handleFatalError(message: message, file: file, line: line)
    }
}

// MARK: - KiliaroSceneViewControllerProtocol

open class KiliaroSceneViewControllerProtocolMock: KiliaroSceneViewControllerProtocol, Mock {
    public init(sequencing sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst, stubbing stubbingPolicy: StubbingPolicy = .wrap, file: StaticString = #file, line: UInt = #line) {
        SwiftyMockyTestObserver.setup()
        self.sequencingPolicy = sequencingPolicy
        self.stubbingPolicy = stubbingPolicy
        self.file = file
        self.line = line
    }

    var matcher: Matcher = Matcher.default
    var stubbingPolicy: StubbingPolicy = .wrap
    var sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst

    private var queue = DispatchQueue(label: "com.swiftymocky.invocations", qos: .userInteractive)
    private var invocations: [MethodType] = []
    private var methodReturnValues: [Given] = []
    private var methodPerformValues: [Perform] = []
    private var file: StaticString?
    private var line: UInt?

    public typealias PropertyStub = Given
    public typealias MethodStub = Given
    public typealias SubscriptStub = Given

    /// Convenience method - call setupMock() to extend debug information when failure occurs
    public func setupMock(file: StaticString = #file, line: UInt = #line) {
        self.file = file
        self.line = line
    }

    /// Clear mock internals. You can specify what to reset (invocations aka verify, givens or performs) or leave it empty to clear all mock internals
    public func resetMock(_ scopes: MockScope...) {
        let scopes: [MockScope] = scopes.isEmpty ? [.invocation, .given, .perform] : scopes
        if scopes.contains(.invocation) { invocations = [] }
        if scopes.contains(.given) { methodReturnValues = [] }
        if scopes.contains(.perform) { methodPerformValues = [] }
    }

    public var viewController: KiliaroSceneViewController! {
		get {	invocations.append(.p_viewController_get); return __p_viewController ?? optionalGivenGetterValue(.p_viewController_get, "KiliaroSceneViewControllerProtocolMock - stub value for viewController was not defined") }
	}
	private var __p_viewController: (KiliaroSceneViewController)?






    fileprivate enum MethodType {
        case p_viewController_get

        static func compareParameters(lhs: MethodType, rhs: MethodType, matcher: Matcher) -> Matcher.ComparisonResult {
            switch (lhs, rhs) {            case (.p_viewController_get,.p_viewController_get): return Matcher.ComparisonResult.match
            }
        }

        func intValue() -> Int {
            switch self {
            case .p_viewController_get: return 0
            }
        }
        func assertionName() -> String {
            switch self {
            case .p_viewController_get: return "[get] .viewController"
            }
        }
    }

    open class Given: StubbedMethod {
        fileprivate var method: MethodType

        private init(method: MethodType, products: [StubProduct]) {
            self.method = method
            super.init(products)
        }

        public static func viewController(getter defaultValue: KiliaroSceneViewController?...) -> PropertyStub {
            return Given(method: .p_viewController_get, products: defaultValue.map({ StubProduct.return($0 as Any) }))
        }

    }

    public struct Verify {
        fileprivate var method: MethodType

        public static var viewController: Verify { return Verify(method: .p_viewController_get) }
    }

    public struct Perform {
        fileprivate var method: MethodType
        var performs: Any

    }

    public func given(_ method: Given) {
        methodReturnValues.append(method)
    }

    public func perform(_ method: Perform) {
        methodPerformValues.append(method)
        methodPerformValues.sort { $0.method.intValue() < $1.method.intValue() }
    }

    public func verify(_ method: Verify, count: Count = Count.moreOrEqual(to: 1), file: StaticString = #file, line: UInt = #line) {
        let fullMatches = matchingCalls(method, file: file, line: line)
        let success = count.matches(fullMatches)
        let assertionName = method.method.assertionName()
        let feedback: String = {
            guard !success else { return "" }
            return Utils.closestCallsMessage(
                for: self.invocations.map { invocation in
                    matcher.set(file: file, line: line)
                    defer { matcher.clearFileAndLine() }
                    return MethodType.compareParameters(lhs: invocation, rhs: method.method, matcher: matcher)
                },
                name: assertionName
            )
        }()
        MockyAssert(success, "Expected: \(count) invocations of `\(assertionName)`, but was: \(fullMatches).\(feedback)", file: file, line: line)
    }

    private func addInvocation(_ call: MethodType) {
        self.queue.sync { invocations.append(call) }
    }
    private func methodReturnValue(_ method: MethodType) throws -> StubProduct {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let candidates = sequencingPolicy.sorted(methodReturnValues, by: { $0.method.intValue() > $1.method.intValue() })
        let matched = candidates.first(where: { $0.isValid && MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch })
        guard let product = matched?.getProduct(policy: self.stubbingPolicy) else { throw MockError.notStubed }
        return product
    }
    private func methodPerformValue(_ method: MethodType) -> Any? {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let matched = methodPerformValues.reversed().first { MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch }
        return matched?.performs
    }
    private func matchingCalls(_ method: MethodType, file: StaticString?, line: UInt?) -> [MethodType] {
        matcher.set(file: file ?? self.file, line: line ?? self.line)
        defer { matcher.clearFileAndLine() }
        return invocations.filter { MethodType.compareParameters(lhs: $0, rhs: method, matcher: matcher).isFullMatch }
    }
    private func matchingCalls(_ method: Verify, file: StaticString?, line: UInt?) -> Int {
        return matchingCalls(method.method, file: file, line: line).count
    }
    private func givenGetterValue<T>(_ method: MethodType, _ message: String) -> T {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            onFatalFailure(message)
            Failure(message)
        }
    }
    private func optionalGivenGetterValue<T>(_ method: MethodType, _ message: String) -> T? {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            return nil
        }
    }
    private func onFatalFailure(_ message: String) {
        guard let file = self.file, let line = self.line else { return } // Let if fail if cannot handle gratefully
        SwiftyMockyTestObserver.handleFatalError(message: message, file: file, line: line)
    }
}

// MARK: - PhotoDetailInteractorProtocol

open class PhotoDetailInteractorProtocolMock: PhotoDetailInteractorProtocol, Mock {
    public init(sequencing sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst, stubbing stubbingPolicy: StubbingPolicy = .wrap, file: StaticString = #file, line: UInt = #line) {
        SwiftyMockyTestObserver.setup()
        self.sequencingPolicy = sequencingPolicy
        self.stubbingPolicy = stubbingPolicy
        self.file = file
        self.line = line
    }

    var matcher: Matcher = Matcher.default
    var stubbingPolicy: StubbingPolicy = .wrap
    var sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst

    private var queue = DispatchQueue(label: "com.swiftymocky.invocations", qos: .userInteractive)
    private var invocations: [MethodType] = []
    private var methodReturnValues: [Given] = []
    private var methodPerformValues: [Perform] = []
    private var file: StaticString?
    private var line: UInt?

    public typealias PropertyStub = Given
    public typealias MethodStub = Given
    public typealias SubscriptStub = Given

    /// Convenience method - call setupMock() to extend debug information when failure occurs
    public func setupMock(file: StaticString = #file, line: UInt = #line) {
        self.file = file
        self.line = line
    }

    /// Clear mock internals. You can specify what to reset (invocations aka verify, givens or performs) or leave it empty to clear all mock internals
    public func resetMock(_ scopes: MockScope...) {
        let scopes: [MockScope] = scopes.isEmpty ? [.invocation, .given, .perform] : scopes
        if scopes.contains(.invocation) { invocations = [] }
        if scopes.contains(.given) { methodReturnValues = [] }
        if scopes.contains(.perform) { methodPerformValues = [] }
    }





    open func didSelectUserProfile() {
        addInvocation(.m_didSelectUserProfile)
		let perform = methodPerformValue(.m_didSelectUserProfile) as? () -> Void
		perform?()
    }

    open func viewDidLoad() {
        addInvocation(.m_viewDidLoad)
		let perform = methodPerformValue(.m_viewDidLoad) as? () -> Void
		perform?()
    }

    open func viewWillAppear(_ animated: Bool) {
        addInvocation(.m_viewWillAppear__animated(Parameter<Bool>.value(`animated`)))
		let perform = methodPerformValue(.m_viewWillAppear__animated(Parameter<Bool>.value(`animated`))) as? (Bool) -> Void
		perform?(`animated`)
    }

    open func viewDidAppear(_ animated: Bool) {
        addInvocation(.m_viewDidAppear__animated(Parameter<Bool>.value(`animated`)))
		let perform = methodPerformValue(.m_viewDidAppear__animated(Parameter<Bool>.value(`animated`))) as? (Bool) -> Void
		perform?(`animated`)
    }

    open func viewWillDisappear(_ animated: Bool) {
        addInvocation(.m_viewWillDisappear__animated(Parameter<Bool>.value(`animated`)))
		let perform = methodPerformValue(.m_viewWillDisappear__animated(Parameter<Bool>.value(`animated`))) as? (Bool) -> Void
		perform?(`animated`)
    }

    open func viewDidDisappear(_ animated: Bool) {
        addInvocation(.m_viewDidDisappear__animated(Parameter<Bool>.value(`animated`)))
		let perform = methodPerformValue(.m_viewDidDisappear__animated(Parameter<Bool>.value(`animated`))) as? (Bool) -> Void
		perform?(`animated`)
    }


    fileprivate enum MethodType {
        case m_didSelectUserProfile
        case m_viewDidLoad
        case m_viewWillAppear__animated(Parameter<Bool>)
        case m_viewDidAppear__animated(Parameter<Bool>)
        case m_viewWillDisappear__animated(Parameter<Bool>)
        case m_viewDidDisappear__animated(Parameter<Bool>)

        static func compareParameters(lhs: MethodType, rhs: MethodType, matcher: Matcher) -> Matcher.ComparisonResult {
            switch (lhs, rhs) {
            case (.m_didSelectUserProfile, .m_didSelectUserProfile): return .match

            case (.m_viewDidLoad, .m_viewDidLoad): return .match

            case (.m_viewWillAppear__animated(let lhsAnimated), .m_viewWillAppear__animated(let rhsAnimated)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsAnimated, rhs: rhsAnimated, with: matcher), lhsAnimated, rhsAnimated, "_ animated"))
				return Matcher.ComparisonResult(results)

            case (.m_viewDidAppear__animated(let lhsAnimated), .m_viewDidAppear__animated(let rhsAnimated)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsAnimated, rhs: rhsAnimated, with: matcher), lhsAnimated, rhsAnimated, "_ animated"))
				return Matcher.ComparisonResult(results)

            case (.m_viewWillDisappear__animated(let lhsAnimated), .m_viewWillDisappear__animated(let rhsAnimated)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsAnimated, rhs: rhsAnimated, with: matcher), lhsAnimated, rhsAnimated, "_ animated"))
				return Matcher.ComparisonResult(results)

            case (.m_viewDidDisappear__animated(let lhsAnimated), .m_viewDidDisappear__animated(let rhsAnimated)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsAnimated, rhs: rhsAnimated, with: matcher), lhsAnimated, rhsAnimated, "_ animated"))
				return Matcher.ComparisonResult(results)
            default: return .none
            }
        }

        func intValue() -> Int {
            switch self {
            case .m_didSelectUserProfile: return 0
            case .m_viewDidLoad: return 0
            case let .m_viewWillAppear__animated(p0): return p0.intValue
            case let .m_viewDidAppear__animated(p0): return p0.intValue
            case let .m_viewWillDisappear__animated(p0): return p0.intValue
            case let .m_viewDidDisappear__animated(p0): return p0.intValue
            }
        }
        func assertionName() -> String {
            switch self {
            case .m_didSelectUserProfile: return ".didSelectUserProfile()"
            case .m_viewDidLoad: return ".viewDidLoad()"
            case .m_viewWillAppear__animated: return ".viewWillAppear(_:)"
            case .m_viewDidAppear__animated: return ".viewDidAppear(_:)"
            case .m_viewWillDisappear__animated: return ".viewWillDisappear(_:)"
            case .m_viewDidDisappear__animated: return ".viewDidDisappear(_:)"
            }
        }
    }

    open class Given: StubbedMethod {
        fileprivate var method: MethodType

        private init(method: MethodType, products: [StubProduct]) {
            self.method = method
            super.init(products)
        }


    }

    public struct Verify {
        fileprivate var method: MethodType

        public static func didSelectUserProfile() -> Verify { return Verify(method: .m_didSelectUserProfile)}
        public static func viewDidLoad() -> Verify { return Verify(method: .m_viewDidLoad)}
        public static func viewWillAppear(_ animated: Parameter<Bool>) -> Verify { return Verify(method: .m_viewWillAppear__animated(`animated`))}
        public static func viewDidAppear(_ animated: Parameter<Bool>) -> Verify { return Verify(method: .m_viewDidAppear__animated(`animated`))}
        public static func viewWillDisappear(_ animated: Parameter<Bool>) -> Verify { return Verify(method: .m_viewWillDisappear__animated(`animated`))}
        public static func viewDidDisappear(_ animated: Parameter<Bool>) -> Verify { return Verify(method: .m_viewDidDisappear__animated(`animated`))}
    }

    public struct Perform {
        fileprivate var method: MethodType
        var performs: Any

        public static func didSelectUserProfile(perform: @escaping () -> Void) -> Perform {
            return Perform(method: .m_didSelectUserProfile, performs: perform)
        }
        public static func viewDidLoad(perform: @escaping () -> Void) -> Perform {
            return Perform(method: .m_viewDidLoad, performs: perform)
        }
        public static func viewWillAppear(_ animated: Parameter<Bool>, perform: @escaping (Bool) -> Void) -> Perform {
            return Perform(method: .m_viewWillAppear__animated(`animated`), performs: perform)
        }
        public static func viewDidAppear(_ animated: Parameter<Bool>, perform: @escaping (Bool) -> Void) -> Perform {
            return Perform(method: .m_viewDidAppear__animated(`animated`), performs: perform)
        }
        public static func viewWillDisappear(_ animated: Parameter<Bool>, perform: @escaping (Bool) -> Void) -> Perform {
            return Perform(method: .m_viewWillDisappear__animated(`animated`), performs: perform)
        }
        public static func viewDidDisappear(_ animated: Parameter<Bool>, perform: @escaping (Bool) -> Void) -> Perform {
            return Perform(method: .m_viewDidDisappear__animated(`animated`), performs: perform)
        }
    }

    public func given(_ method: Given) {
        methodReturnValues.append(method)
    }

    public func perform(_ method: Perform) {
        methodPerformValues.append(method)
        methodPerformValues.sort { $0.method.intValue() < $1.method.intValue() }
    }

    public func verify(_ method: Verify, count: Count = Count.moreOrEqual(to: 1), file: StaticString = #file, line: UInt = #line) {
        let fullMatches = matchingCalls(method, file: file, line: line)
        let success = count.matches(fullMatches)
        let assertionName = method.method.assertionName()
        let feedback: String = {
            guard !success else { return "" }
            return Utils.closestCallsMessage(
                for: self.invocations.map { invocation in
                    matcher.set(file: file, line: line)
                    defer { matcher.clearFileAndLine() }
                    return MethodType.compareParameters(lhs: invocation, rhs: method.method, matcher: matcher)
                },
                name: assertionName
            )
        }()
        MockyAssert(success, "Expected: \(count) invocations of `\(assertionName)`, but was: \(fullMatches).\(feedback)", file: file, line: line)
    }

    private func addInvocation(_ call: MethodType) {
        self.queue.sync { invocations.append(call) }
    }
    private func methodReturnValue(_ method: MethodType) throws -> StubProduct {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let candidates = sequencingPolicy.sorted(methodReturnValues, by: { $0.method.intValue() > $1.method.intValue() })
        let matched = candidates.first(where: { $0.isValid && MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch })
        guard let product = matched?.getProduct(policy: self.stubbingPolicy) else { throw MockError.notStubed }
        return product
    }
    private func methodPerformValue(_ method: MethodType) -> Any? {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let matched = methodPerformValues.reversed().first { MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch }
        return matched?.performs
    }
    private func matchingCalls(_ method: MethodType, file: StaticString?, line: UInt?) -> [MethodType] {
        matcher.set(file: file ?? self.file, line: line ?? self.line)
        defer { matcher.clearFileAndLine() }
        return invocations.filter { MethodType.compareParameters(lhs: $0, rhs: method, matcher: matcher).isFullMatch }
    }
    private func matchingCalls(_ method: Verify, file: StaticString?, line: UInt?) -> Int {
        return matchingCalls(method.method, file: file, line: line).count
    }
    private func givenGetterValue<T>(_ method: MethodType, _ message: String) -> T {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            onFatalFailure(message)
            Failure(message)
        }
    }
    private func optionalGivenGetterValue<T>(_ method: MethodType, _ message: String) -> T? {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            return nil
        }
    }
    private func onFatalFailure(_ message: String) {
        guard let file = self.file, let line = self.line else { return } // Let if fail if cannot handle gratefully
        SwiftyMockyTestObserver.handleFatalError(message: message, file: file, line: line)
    }
}

// MARK: - PhotoDetailPresenterProtocol

open class PhotoDetailPresenterProtocolMock: PhotoDetailPresenterProtocol, Mock {
    public init(sequencing sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst, stubbing stubbingPolicy: StubbingPolicy = .wrap, file: StaticString = #file, line: UInt = #line) {
        SwiftyMockyTestObserver.setup()
        self.sequencingPolicy = sequencingPolicy
        self.stubbingPolicy = stubbingPolicy
        self.file = file
        self.line = line
    }

    var matcher: Matcher = Matcher.default
    var stubbingPolicy: StubbingPolicy = .wrap
    var sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst

    private var queue = DispatchQueue(label: "com.swiftymocky.invocations", qos: .userInteractive)
    private var invocations: [MethodType] = []
    private var methodReturnValues: [Given] = []
    private var methodPerformValues: [Perform] = []
    private var file: StaticString?
    private var line: UInt?

    public typealias PropertyStub = Given
    public typealias MethodStub = Given
    public typealias SubscriptStub = Given

    /// Convenience method - call setupMock() to extend debug information when failure occurs
    public func setupMock(file: StaticString = #file, line: UInt = #line) {
        self.file = file
        self.line = line
    }

    /// Clear mock internals. You can specify what to reset (invocations aka verify, givens or performs) or leave it empty to clear all mock internals
    public func resetMock(_ scopes: MockScope...) {
        let scopes: [MockScope] = scopes.isEmpty ? [.invocation, .given, .perform] : scopes
        if scopes.contains(.invocation) { invocations = [] }
        if scopes.contains(.given) { methodReturnValues = [] }
        if scopes.contains(.perform) { methodPerformValues = [] }
    }





    open func presentPhoto(photo: Media) {
        addInvocation(.m_presentPhoto__photo_photo(Parameter<Media>.value(`photo`)))
		let perform = methodPerformValue(.m_presentPhoto__photo_photo(Parameter<Media>.value(`photo`))) as? (Media) -> Void
		perform?(`photo`)
    }

    open func handle(error: KiliaroError?) {
        addInvocation(.m_handle__error_error(Parameter<KiliaroError?>.value(`error`)))
		let perform = methodPerformValue(.m_handle__error_error(Parameter<KiliaroError?>.value(`error`))) as? (KiliaroError?) -> Void
		perform?(`error`)
    }

    open func handle(error: KiliaroError?, dismiss: (() -> Void)?) {
        addInvocation(.m_handle__error_errordismiss_dismiss(Parameter<KiliaroError?>.value(`error`), Parameter<(() -> Void)?>.value(`dismiss`)))
		let perform = methodPerformValue(.m_handle__error_errordismiss_dismiss(Parameter<KiliaroError?>.value(`error`), Parameter<(() -> Void)?>.value(`dismiss`))) as? (KiliaroError?, (() -> Void)?) -> Void
		perform?(`error`, `dismiss`)
    }


    fileprivate enum MethodType {
        case m_presentPhoto__photo_photo(Parameter<Media>)
        case m_handle__error_error(Parameter<KiliaroError?>)
        case m_handle__error_errordismiss_dismiss(Parameter<KiliaroError?>, Parameter<(() -> Void)?>)

        static func compareParameters(lhs: MethodType, rhs: MethodType, matcher: Matcher) -> Matcher.ComparisonResult {
            switch (lhs, rhs) {
            case (.m_presentPhoto__photo_photo(let lhsPhoto), .m_presentPhoto__photo_photo(let rhsPhoto)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsPhoto, rhs: rhsPhoto, with: matcher), lhsPhoto, rhsPhoto, "photo"))
				return Matcher.ComparisonResult(results)

            case (.m_handle__error_error(let lhsError), .m_handle__error_error(let rhsError)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsError, rhs: rhsError, with: matcher), lhsError, rhsError, "error"))
				return Matcher.ComparisonResult(results)

            case (.m_handle__error_errordismiss_dismiss(let lhsError, let lhsDismiss), .m_handle__error_errordismiss_dismiss(let rhsError, let rhsDismiss)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsError, rhs: rhsError, with: matcher), lhsError, rhsError, "error"))
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsDismiss, rhs: rhsDismiss, with: matcher), lhsDismiss, rhsDismiss, "dismiss"))
				return Matcher.ComparisonResult(results)
            default: return .none
            }
        }

        func intValue() -> Int {
            switch self {
            case let .m_presentPhoto__photo_photo(p0): return p0.intValue
            case let .m_handle__error_error(p0): return p0.intValue
            case let .m_handle__error_errordismiss_dismiss(p0, p1): return p0.intValue + p1.intValue
            }
        }
        func assertionName() -> String {
            switch self {
            case .m_presentPhoto__photo_photo: return ".presentPhoto(photo:)"
            case .m_handle__error_error: return ".handle(error:)"
            case .m_handle__error_errordismiss_dismiss: return ".handle(error:dismiss:)"
            }
        }
    }

    open class Given: StubbedMethod {
        fileprivate var method: MethodType

        private init(method: MethodType, products: [StubProduct]) {
            self.method = method
            super.init(products)
        }


    }

    public struct Verify {
        fileprivate var method: MethodType

        public static func presentPhoto(photo: Parameter<Media>) -> Verify { return Verify(method: .m_presentPhoto__photo_photo(`photo`))}
        public static func handle(error: Parameter<KiliaroError?>) -> Verify { return Verify(method: .m_handle__error_error(`error`))}
        public static func handle(error: Parameter<KiliaroError?>, dismiss: Parameter<(() -> Void)?>) -> Verify { return Verify(method: .m_handle__error_errordismiss_dismiss(`error`, `dismiss`))}
    }

    public struct Perform {
        fileprivate var method: MethodType
        var performs: Any

        public static func presentPhoto(photo: Parameter<Media>, perform: @escaping (Media) -> Void) -> Perform {
            return Perform(method: .m_presentPhoto__photo_photo(`photo`), performs: perform)
        }
        public static func handle(error: Parameter<KiliaroError?>, perform: @escaping (KiliaroError?) -> Void) -> Perform {
            return Perform(method: .m_handle__error_error(`error`), performs: perform)
        }
        public static func handle(error: Parameter<KiliaroError?>, dismiss: Parameter<(() -> Void)?>, perform: @escaping (KiliaroError?, (() -> Void)?) -> Void) -> Perform {
            return Perform(method: .m_handle__error_errordismiss_dismiss(`error`, `dismiss`), performs: perform)
        }
    }

    public func given(_ method: Given) {
        methodReturnValues.append(method)
    }

    public func perform(_ method: Perform) {
        methodPerformValues.append(method)
        methodPerformValues.sort { $0.method.intValue() < $1.method.intValue() }
    }

    public func verify(_ method: Verify, count: Count = Count.moreOrEqual(to: 1), file: StaticString = #file, line: UInt = #line) {
        let fullMatches = matchingCalls(method, file: file, line: line)
        let success = count.matches(fullMatches)
        let assertionName = method.method.assertionName()
        let feedback: String = {
            guard !success else { return "" }
            return Utils.closestCallsMessage(
                for: self.invocations.map { invocation in
                    matcher.set(file: file, line: line)
                    defer { matcher.clearFileAndLine() }
                    return MethodType.compareParameters(lhs: invocation, rhs: method.method, matcher: matcher)
                },
                name: assertionName
            )
        }()
        MockyAssert(success, "Expected: \(count) invocations of `\(assertionName)`, but was: \(fullMatches).\(feedback)", file: file, line: line)
    }

    private func addInvocation(_ call: MethodType) {
        self.queue.sync { invocations.append(call) }
    }
    private func methodReturnValue(_ method: MethodType) throws -> StubProduct {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let candidates = sequencingPolicy.sorted(methodReturnValues, by: { $0.method.intValue() > $1.method.intValue() })
        let matched = candidates.first(where: { $0.isValid && MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch })
        guard let product = matched?.getProduct(policy: self.stubbingPolicy) else { throw MockError.notStubed }
        return product
    }
    private func methodPerformValue(_ method: MethodType) -> Any? {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let matched = methodPerformValues.reversed().first { MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch }
        return matched?.performs
    }
    private func matchingCalls(_ method: MethodType, file: StaticString?, line: UInt?) -> [MethodType] {
        matcher.set(file: file ?? self.file, line: line ?? self.line)
        defer { matcher.clearFileAndLine() }
        return invocations.filter { MethodType.compareParameters(lhs: $0, rhs: method, matcher: matcher).isFullMatch }
    }
    private func matchingCalls(_ method: Verify, file: StaticString?, line: UInt?) -> Int {
        return matchingCalls(method.method, file: file, line: line).count
    }
    private func givenGetterValue<T>(_ method: MethodType, _ message: String) -> T {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            onFatalFailure(message)
            Failure(message)
        }
    }
    private func optionalGivenGetterValue<T>(_ method: MethodType, _ message: String) -> T? {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            return nil
        }
    }
    private func onFatalFailure(_ message: String) {
        guard let file = self.file, let line = self.line else { return } // Let if fail if cannot handle gratefully
        SwiftyMockyTestObserver.handleFatalError(message: message, file: file, line: line)
    }
}

// MARK: - PhotoDetailRouterProtocol

open class PhotoDetailRouterProtocolMock: PhotoDetailRouterProtocol, Mock {
    public init(sequencing sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst, stubbing stubbingPolicy: StubbingPolicy = .wrap, file: StaticString = #file, line: UInt = #line) {
        SwiftyMockyTestObserver.setup()
        self.sequencingPolicy = sequencingPolicy
        self.stubbingPolicy = stubbingPolicy
        self.file = file
        self.line = line
    }

    var matcher: Matcher = Matcher.default
    var stubbingPolicy: StubbingPolicy = .wrap
    var sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst

    private var queue = DispatchQueue(label: "com.swiftymocky.invocations", qos: .userInteractive)
    private var invocations: [MethodType] = []
    private var methodReturnValues: [Given] = []
    private var methodPerformValues: [Perform] = []
    private var file: StaticString?
    private var line: UInt?

    public typealias PropertyStub = Given
    public typealias MethodStub = Given
    public typealias SubscriptStub = Given

    /// Convenience method - call setupMock() to extend debug information when failure occurs
    public func setupMock(file: StaticString = #file, line: UInt = #line) {
        self.file = file
        self.line = line
    }

    /// Clear mock internals. You can specify what to reset (invocations aka verify, givens or performs) or leave it empty to clear all mock internals
    public func resetMock(_ scopes: MockScope...) {
        let scopes: [MockScope] = scopes.isEmpty ? [.invocation, .given, .perform] : scopes
        if scopes.contains(.invocation) { invocations = [] }
        if scopes.contains(.given) { methodReturnValues = [] }
        if scopes.contains(.perform) { methodPerformValues = [] }
    }





    open func dismiss() {
        addInvocation(.m_dismiss)
		let perform = methodPerformValue(.m_dismiss) as? () -> Void
		perform?()
    }


    fileprivate enum MethodType {
        case m_dismiss

        static func compareParameters(lhs: MethodType, rhs: MethodType, matcher: Matcher) -> Matcher.ComparisonResult {
            switch (lhs, rhs) {
            case (.m_dismiss, .m_dismiss): return .match
            }
        }

        func intValue() -> Int {
            switch self {
            case .m_dismiss: return 0
            }
        }
        func assertionName() -> String {
            switch self {
            case .m_dismiss: return ".dismiss()"
            }
        }
    }

    open class Given: StubbedMethod {
        fileprivate var method: MethodType

        private init(method: MethodType, products: [StubProduct]) {
            self.method = method
            super.init(products)
        }


    }

    public struct Verify {
        fileprivate var method: MethodType

        public static func dismiss() -> Verify { return Verify(method: .m_dismiss)}
    }

    public struct Perform {
        fileprivate var method: MethodType
        var performs: Any

        public static func dismiss(perform: @escaping () -> Void) -> Perform {
            return Perform(method: .m_dismiss, performs: perform)
        }
    }

    public func given(_ method: Given) {
        methodReturnValues.append(method)
    }

    public func perform(_ method: Perform) {
        methodPerformValues.append(method)
        methodPerformValues.sort { $0.method.intValue() < $1.method.intValue() }
    }

    public func verify(_ method: Verify, count: Count = Count.moreOrEqual(to: 1), file: StaticString = #file, line: UInt = #line) {
        let fullMatches = matchingCalls(method, file: file, line: line)
        let success = count.matches(fullMatches)
        let assertionName = method.method.assertionName()
        let feedback: String = {
            guard !success else { return "" }
            return Utils.closestCallsMessage(
                for: self.invocations.map { invocation in
                    matcher.set(file: file, line: line)
                    defer { matcher.clearFileAndLine() }
                    return MethodType.compareParameters(lhs: invocation, rhs: method.method, matcher: matcher)
                },
                name: assertionName
            )
        }()
        MockyAssert(success, "Expected: \(count) invocations of `\(assertionName)`, but was: \(fullMatches).\(feedback)", file: file, line: line)
    }

    private func addInvocation(_ call: MethodType) {
        self.queue.sync { invocations.append(call) }
    }
    private func methodReturnValue(_ method: MethodType) throws -> StubProduct {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let candidates = sequencingPolicy.sorted(methodReturnValues, by: { $0.method.intValue() > $1.method.intValue() })
        let matched = candidates.first(where: { $0.isValid && MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch })
        guard let product = matched?.getProduct(policy: self.stubbingPolicy) else { throw MockError.notStubed }
        return product
    }
    private func methodPerformValue(_ method: MethodType) -> Any? {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let matched = methodPerformValues.reversed().first { MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch }
        return matched?.performs
    }
    private func matchingCalls(_ method: MethodType, file: StaticString?, line: UInt?) -> [MethodType] {
        matcher.set(file: file ?? self.file, line: line ?? self.line)
        defer { matcher.clearFileAndLine() }
        return invocations.filter { MethodType.compareParameters(lhs: $0, rhs: method, matcher: matcher).isFullMatch }
    }
    private func matchingCalls(_ method: Verify, file: StaticString?, line: UInt?) -> Int {
        return matchingCalls(method.method, file: file, line: line).count
    }
    private func givenGetterValue<T>(_ method: MethodType, _ message: String) -> T {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            onFatalFailure(message)
            Failure(message)
        }
    }
    private func optionalGivenGetterValue<T>(_ method: MethodType, _ message: String) -> T? {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            return nil
        }
    }
    private func onFatalFailure(_ message: String) {
        guard let file = self.file, let line = self.line else { return } // Let if fail if cannot handle gratefully
        SwiftyMockyTestObserver.handleFatalError(message: message, file: file, line: line)
    }
}

// MARK: - PhotoDetailViewControllerProtocol

open class PhotoDetailViewControllerProtocolMock: PhotoDetailViewControllerProtocol, Mock {
    public init(sequencing sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst, stubbing stubbingPolicy: StubbingPolicy = .wrap, file: StaticString = #file, line: UInt = #line) {
        SwiftyMockyTestObserver.setup()
        self.sequencingPolicy = sequencingPolicy
        self.stubbingPolicy = stubbingPolicy
        self.file = file
        self.line = line
    }

    var matcher: Matcher = Matcher.default
    var stubbingPolicy: StubbingPolicy = .wrap
    var sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst

    private var queue = DispatchQueue(label: "com.swiftymocky.invocations", qos: .userInteractive)
    private var invocations: [MethodType] = []
    private var methodReturnValues: [Given] = []
    private var methodPerformValues: [Perform] = []
    private var file: StaticString?
    private var line: UInt?

    public typealias PropertyStub = Given
    public typealias MethodStub = Given
    public typealias SubscriptStub = Given

    /// Convenience method - call setupMock() to extend debug information when failure occurs
    public func setupMock(file: StaticString = #file, line: UInt = #line) {
        self.file = file
        self.line = line
    }

    /// Clear mock internals. You can specify what to reset (invocations aka verify, givens or performs) or leave it empty to clear all mock internals
    public func resetMock(_ scopes: MockScope...) {
        let scopes: [MockScope] = scopes.isEmpty ? [.invocation, .given, .perform] : scopes
        if scopes.contains(.invocation) { invocations = [] }
        if scopes.contains(.given) { methodReturnValues = [] }
        if scopes.contains(.perform) { methodPerformValues = [] }
    }

    public var viewController: KiliaroSceneViewController! {
		get {	invocations.append(.p_viewController_get); return __p_viewController ?? optionalGivenGetterValue(.p_viewController_get, "PhotoDetailViewControllerProtocolMock - stub value for viewController was not defined") }
	}
	private var __p_viewController: (KiliaroSceneViewController)?





    open func showError(_ error: KiliaroError?) {
        addInvocation(.m_showError__error(Parameter<KiliaroError?>.value(`error`)))
		let perform = methodPerformValue(.m_showError__error(Parameter<KiliaroError?>.value(`error`))) as? (KiliaroError?) -> Void
		perform?(`error`)
    }

    open func showPhoto(photo: PhotoViewModel) {
        addInvocation(.m_showPhoto__photo_photo(Parameter<PhotoViewModel>.value(`photo`)))
		let perform = methodPerformValue(.m_showPhoto__photo_photo(Parameter<PhotoViewModel>.value(`photo`))) as? (PhotoViewModel) -> Void
		perform?(`photo`)
    }


    fileprivate enum MethodType {
        case m_showError__error(Parameter<KiliaroError?>)
        case m_showPhoto__photo_photo(Parameter<PhotoViewModel>)
        case p_viewController_get

        static func compareParameters(lhs: MethodType, rhs: MethodType, matcher: Matcher) -> Matcher.ComparisonResult {
            switch (lhs, rhs) {
            case (.m_showError__error(let lhsError), .m_showError__error(let rhsError)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsError, rhs: rhsError, with: matcher), lhsError, rhsError, "_ error"))
				return Matcher.ComparisonResult(results)

            case (.m_showPhoto__photo_photo(let lhsPhoto), .m_showPhoto__photo_photo(let rhsPhoto)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsPhoto, rhs: rhsPhoto, with: matcher), lhsPhoto, rhsPhoto, "photo"))
				return Matcher.ComparisonResult(results)
            case (.p_viewController_get,.p_viewController_get): return Matcher.ComparisonResult.match
            default: return .none
            }
        }

        func intValue() -> Int {
            switch self {
            case let .m_showError__error(p0): return p0.intValue
            case let .m_showPhoto__photo_photo(p0): return p0.intValue
            case .p_viewController_get: return 0
            }
        }
        func assertionName() -> String {
            switch self {
            case .m_showError__error: return ".showError(_:)"
            case .m_showPhoto__photo_photo: return ".showPhoto(photo:)"
            case .p_viewController_get: return "[get] .viewController"
            }
        }
    }

    open class Given: StubbedMethod {
        fileprivate var method: MethodType

        private init(method: MethodType, products: [StubProduct]) {
            self.method = method
            super.init(products)
        }

        public static func viewController(getter defaultValue: KiliaroSceneViewController?...) -> PropertyStub {
            return Given(method: .p_viewController_get, products: defaultValue.map({ StubProduct.return($0 as Any) }))
        }

    }

    public struct Verify {
        fileprivate var method: MethodType

        public static func showError(_ error: Parameter<KiliaroError?>) -> Verify { return Verify(method: .m_showError__error(`error`))}
        public static func showPhoto(photo: Parameter<PhotoViewModel>) -> Verify { return Verify(method: .m_showPhoto__photo_photo(`photo`))}
        public static var viewController: Verify { return Verify(method: .p_viewController_get) }
    }

    public struct Perform {
        fileprivate var method: MethodType
        var performs: Any

        public static func showError(_ error: Parameter<KiliaroError?>, perform: @escaping (KiliaroError?) -> Void) -> Perform {
            return Perform(method: .m_showError__error(`error`), performs: perform)
        }
        public static func showPhoto(photo: Parameter<PhotoViewModel>, perform: @escaping (PhotoViewModel) -> Void) -> Perform {
            return Perform(method: .m_showPhoto__photo_photo(`photo`), performs: perform)
        }
    }

    public func given(_ method: Given) {
        methodReturnValues.append(method)
    }

    public func perform(_ method: Perform) {
        methodPerformValues.append(method)
        methodPerformValues.sort { $0.method.intValue() < $1.method.intValue() }
    }

    public func verify(_ method: Verify, count: Count = Count.moreOrEqual(to: 1), file: StaticString = #file, line: UInt = #line) {
        let fullMatches = matchingCalls(method, file: file, line: line)
        let success = count.matches(fullMatches)
        let assertionName = method.method.assertionName()
        let feedback: String = {
            guard !success else { return "" }
            return Utils.closestCallsMessage(
                for: self.invocations.map { invocation in
                    matcher.set(file: file, line: line)
                    defer { matcher.clearFileAndLine() }
                    return MethodType.compareParameters(lhs: invocation, rhs: method.method, matcher: matcher)
                },
                name: assertionName
            )
        }()
        MockyAssert(success, "Expected: \(count) invocations of `\(assertionName)`, but was: \(fullMatches).\(feedback)", file: file, line: line)
    }

    private func addInvocation(_ call: MethodType) {
        self.queue.sync { invocations.append(call) }
    }
    private func methodReturnValue(_ method: MethodType) throws -> StubProduct {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let candidates = sequencingPolicy.sorted(methodReturnValues, by: { $0.method.intValue() > $1.method.intValue() })
        let matched = candidates.first(where: { $0.isValid && MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch })
        guard let product = matched?.getProduct(policy: self.stubbingPolicy) else { throw MockError.notStubed }
        return product
    }
    private func methodPerformValue(_ method: MethodType) -> Any? {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let matched = methodPerformValues.reversed().first { MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch }
        return matched?.performs
    }
    private func matchingCalls(_ method: MethodType, file: StaticString?, line: UInt?) -> [MethodType] {
        matcher.set(file: file ?? self.file, line: line ?? self.line)
        defer { matcher.clearFileAndLine() }
        return invocations.filter { MethodType.compareParameters(lhs: $0, rhs: method, matcher: matcher).isFullMatch }
    }
    private func matchingCalls(_ method: Verify, file: StaticString?, line: UInt?) -> Int {
        return matchingCalls(method.method, file: file, line: line).count
    }
    private func givenGetterValue<T>(_ method: MethodType, _ message: String) -> T {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            onFatalFailure(message)
            Failure(message)
        }
    }
    private func optionalGivenGetterValue<T>(_ method: MethodType, _ message: String) -> T? {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            return nil
        }
    }
    private func onFatalFailure(_ message: String) {
        guard let file = self.file, let line = self.line else { return } // Let if fail if cannot handle gratefully
        SwiftyMockyTestObserver.handleFatalError(message: message, file: file, line: line)
    }
}

// MARK: - PhotosContentManagerProtocol

open class PhotosContentManagerProtocolMock: PhotosContentManagerProtocol, Mock {
    public init(sequencing sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst, stubbing stubbingPolicy: StubbingPolicy = .wrap, file: StaticString = #file, line: UInt = #line) {
        SwiftyMockyTestObserver.setup()
        self.sequencingPolicy = sequencingPolicy
        self.stubbingPolicy = stubbingPolicy
        self.file = file
        self.line = line
    }

    var matcher: Matcher = Matcher.default
    var stubbingPolicy: StubbingPolicy = .wrap
    var sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst

    private var queue = DispatchQueue(label: "com.swiftymocky.invocations", qos: .userInteractive)
    private var invocations: [MethodType] = []
    private var methodReturnValues: [Given] = []
    private var methodPerformValues: [Perform] = []
    private var file: StaticString?
    private var line: UInt?

    public typealias PropertyStub = Given
    public typealias MethodStub = Given
    public typealias SubscriptStub = Given

    /// Convenience method - call setupMock() to extend debug information when failure occurs
    public func setupMock(file: StaticString = #file, line: UInt = #line) {
        self.file = file
        self.line = line
    }

    /// Clear mock internals. You can specify what to reset (invocations aka verify, givens or performs) or leave it empty to clear all mock internals
    public func resetMock(_ scopes: MockScope...) {
        let scopes: [MockScope] = scopes.isEmpty ? [.invocation, .given, .perform] : scopes
        if scopes.contains(.invocation) { invocations = [] }
        if scopes.contains(.given) { methodReturnValues = [] }
        if scopes.contains(.perform) { methodPerformValues = [] }
    }





    open func requestData(completion: @escaping ([Media]?, KiliaroError?) -> Void) {
        addInvocation(.m_requestData__completion_completion(Parameter<([Media]?, KiliaroError?) -> Void>.value(`completion`)))
		let perform = methodPerformValue(.m_requestData__completion_completion(Parameter<([Media]?, KiliaroError?) -> Void>.value(`completion`))) as? (@escaping ([Media]?, KiliaroError?) -> Void) -> Void
		perform?(`completion`)
    }


    fileprivate enum MethodType {
        case m_requestData__completion_completion(Parameter<([Media]?, KiliaroError?) -> Void>)

        static func compareParameters(lhs: MethodType, rhs: MethodType, matcher: Matcher) -> Matcher.ComparisonResult {
            switch (lhs, rhs) {
            case (.m_requestData__completion_completion(let lhsCompletion), .m_requestData__completion_completion(let rhsCompletion)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsCompletion, rhs: rhsCompletion, with: matcher), lhsCompletion, rhsCompletion, "completion"))
				return Matcher.ComparisonResult(results)
            }
        }

        func intValue() -> Int {
            switch self {
            case let .m_requestData__completion_completion(p0): return p0.intValue
            }
        }
        func assertionName() -> String {
            switch self {
            case .m_requestData__completion_completion: return ".requestData(completion:)"
            }
        }
    }

    open class Given: StubbedMethod {
        fileprivate var method: MethodType

        private init(method: MethodType, products: [StubProduct]) {
            self.method = method
            super.init(products)
        }


    }

    public struct Verify {
        fileprivate var method: MethodType

        public static func requestData(completion: Parameter<([Media]?, KiliaroError?) -> Void>) -> Verify { return Verify(method: .m_requestData__completion_completion(`completion`))}
    }

    public struct Perform {
        fileprivate var method: MethodType
        var performs: Any

        public static func requestData(completion: Parameter<([Media]?, KiliaroError?) -> Void>, perform: @escaping (@escaping ([Media]?, KiliaroError?) -> Void) -> Void) -> Perform {
            return Perform(method: .m_requestData__completion_completion(`completion`), performs: perform)
        }
    }

    public func given(_ method: Given) {
        methodReturnValues.append(method)
    }

    public func perform(_ method: Perform) {
        methodPerformValues.append(method)
        methodPerformValues.sort { $0.method.intValue() < $1.method.intValue() }
    }

    public func verify(_ method: Verify, count: Count = Count.moreOrEqual(to: 1), file: StaticString = #file, line: UInt = #line) {
        let fullMatches = matchingCalls(method, file: file, line: line)
        let success = count.matches(fullMatches)
        let assertionName = method.method.assertionName()
        let feedback: String = {
            guard !success else { return "" }
            return Utils.closestCallsMessage(
                for: self.invocations.map { invocation in
                    matcher.set(file: file, line: line)
                    defer { matcher.clearFileAndLine() }
                    return MethodType.compareParameters(lhs: invocation, rhs: method.method, matcher: matcher)
                },
                name: assertionName
            )
        }()
        MockyAssert(success, "Expected: \(count) invocations of `\(assertionName)`, but was: \(fullMatches).\(feedback)", file: file, line: line)
    }

    private func addInvocation(_ call: MethodType) {
        self.queue.sync { invocations.append(call) }
    }
    private func methodReturnValue(_ method: MethodType) throws -> StubProduct {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let candidates = sequencingPolicy.sorted(methodReturnValues, by: { $0.method.intValue() > $1.method.intValue() })
        let matched = candidates.first(where: { $0.isValid && MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch })
        guard let product = matched?.getProduct(policy: self.stubbingPolicy) else { throw MockError.notStubed }
        return product
    }
    private func methodPerformValue(_ method: MethodType) -> Any? {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let matched = methodPerformValues.reversed().first { MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch }
        return matched?.performs
    }
    private func matchingCalls(_ method: MethodType, file: StaticString?, line: UInt?) -> [MethodType] {
        matcher.set(file: file ?? self.file, line: line ?? self.line)
        defer { matcher.clearFileAndLine() }
        return invocations.filter { MethodType.compareParameters(lhs: $0, rhs: method, matcher: matcher).isFullMatch }
    }
    private func matchingCalls(_ method: Verify, file: StaticString?, line: UInt?) -> Int {
        return matchingCalls(method.method, file: file, line: line).count
    }
    private func givenGetterValue<T>(_ method: MethodType, _ message: String) -> T {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            onFatalFailure(message)
            Failure(message)
        }
    }
    private func optionalGivenGetterValue<T>(_ method: MethodType, _ message: String) -> T? {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            return nil
        }
    }
    private func onFatalFailure(_ message: String) {
        guard let file = self.file, let line = self.line else { return } // Let if fail if cannot handle gratefully
        SwiftyMockyTestObserver.handleFatalError(message: message, file: file, line: line)
    }
}

// MARK: - PhotosInteractorProtocol

open class PhotosInteractorProtocolMock: PhotosInteractorProtocol, Mock {
    public init(sequencing sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst, stubbing stubbingPolicy: StubbingPolicy = .wrap, file: StaticString = #file, line: UInt = #line) {
        SwiftyMockyTestObserver.setup()
        self.sequencingPolicy = sequencingPolicy
        self.stubbingPolicy = stubbingPolicy
        self.file = file
        self.line = line
    }

    var matcher: Matcher = Matcher.default
    var stubbingPolicy: StubbingPolicy = .wrap
    var sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst

    private var queue = DispatchQueue(label: "com.swiftymocky.invocations", qos: .userInteractive)
    private var invocations: [MethodType] = []
    private var methodReturnValues: [Given] = []
    private var methodPerformValues: [Perform] = []
    private var file: StaticString?
    private var line: UInt?

    public typealias PropertyStub = Given
    public typealias MethodStub = Given
    public typealias SubscriptStub = Given

    /// Convenience method - call setupMock() to extend debug information when failure occurs
    public func setupMock(file: StaticString = #file, line: UInt = #line) {
        self.file = file
        self.line = line
    }

    /// Clear mock internals. You can specify what to reset (invocations aka verify, givens or performs) or leave it empty to clear all mock internals
    public func resetMock(_ scopes: MockScope...) {
        let scopes: [MockScope] = scopes.isEmpty ? [.invocation, .given, .perform] : scopes
        if scopes.contains(.invocation) { invocations = [] }
        if scopes.contains(.given) { methodReturnValues = [] }
        if scopes.contains(.perform) { methodPerformValues = [] }
    }





    open func fetchAlbumPhotos() {
        addInvocation(.m_fetchAlbumPhotos)
		let perform = methodPerformValue(.m_fetchAlbumPhotos) as? () -> Void
		perform?()
    }

    open func tapOnSettings() {
        addInvocation(.m_tapOnSettings)
		let perform = methodPerformValue(.m_tapOnSettings) as? () -> Void
		perform?()
    }

    open func tapOnPhoto(photoID: String) {
        addInvocation(.m_tapOnPhoto__photoID_photoID(Parameter<String>.value(`photoID`)))
		let perform = methodPerformValue(.m_tapOnPhoto__photoID_photoID(Parameter<String>.value(`photoID`))) as? (String) -> Void
		perform?(`photoID`)
    }

    open func viewDidLoad() {
        addInvocation(.m_viewDidLoad)
		let perform = methodPerformValue(.m_viewDidLoad) as? () -> Void
		perform?()
    }

    open func viewWillAppear(_ animated: Bool) {
        addInvocation(.m_viewWillAppear__animated(Parameter<Bool>.value(`animated`)))
		let perform = methodPerformValue(.m_viewWillAppear__animated(Parameter<Bool>.value(`animated`))) as? (Bool) -> Void
		perform?(`animated`)
    }

    open func viewDidAppear(_ animated: Bool) {
        addInvocation(.m_viewDidAppear__animated(Parameter<Bool>.value(`animated`)))
		let perform = methodPerformValue(.m_viewDidAppear__animated(Parameter<Bool>.value(`animated`))) as? (Bool) -> Void
		perform?(`animated`)
    }

    open func viewWillDisappear(_ animated: Bool) {
        addInvocation(.m_viewWillDisappear__animated(Parameter<Bool>.value(`animated`)))
		let perform = methodPerformValue(.m_viewWillDisappear__animated(Parameter<Bool>.value(`animated`))) as? (Bool) -> Void
		perform?(`animated`)
    }

    open func viewDidDisappear(_ animated: Bool) {
        addInvocation(.m_viewDidDisappear__animated(Parameter<Bool>.value(`animated`)))
		let perform = methodPerformValue(.m_viewDidDisappear__animated(Parameter<Bool>.value(`animated`))) as? (Bool) -> Void
		perform?(`animated`)
    }


    fileprivate enum MethodType {
        case m_fetchAlbumPhotos
        case m_tapOnSettings
        case m_tapOnPhoto__photoID_photoID(Parameter<String>)
        case m_viewDidLoad
        case m_viewWillAppear__animated(Parameter<Bool>)
        case m_viewDidAppear__animated(Parameter<Bool>)
        case m_viewWillDisappear__animated(Parameter<Bool>)
        case m_viewDidDisappear__animated(Parameter<Bool>)

        static func compareParameters(lhs: MethodType, rhs: MethodType, matcher: Matcher) -> Matcher.ComparisonResult {
            switch (lhs, rhs) {
            case (.m_fetchAlbumPhotos, .m_fetchAlbumPhotos): return .match

            case (.m_tapOnSettings, .m_tapOnSettings): return .match

            case (.m_tapOnPhoto__photoID_photoID(let lhsPhotoid), .m_tapOnPhoto__photoID_photoID(let rhsPhotoid)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsPhotoid, rhs: rhsPhotoid, with: matcher), lhsPhotoid, rhsPhotoid, "photoID"))
				return Matcher.ComparisonResult(results)

            case (.m_viewDidLoad, .m_viewDidLoad): return .match

            case (.m_viewWillAppear__animated(let lhsAnimated), .m_viewWillAppear__animated(let rhsAnimated)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsAnimated, rhs: rhsAnimated, with: matcher), lhsAnimated, rhsAnimated, "_ animated"))
				return Matcher.ComparisonResult(results)

            case (.m_viewDidAppear__animated(let lhsAnimated), .m_viewDidAppear__animated(let rhsAnimated)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsAnimated, rhs: rhsAnimated, with: matcher), lhsAnimated, rhsAnimated, "_ animated"))
				return Matcher.ComparisonResult(results)

            case (.m_viewWillDisappear__animated(let lhsAnimated), .m_viewWillDisappear__animated(let rhsAnimated)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsAnimated, rhs: rhsAnimated, with: matcher), lhsAnimated, rhsAnimated, "_ animated"))
				return Matcher.ComparisonResult(results)

            case (.m_viewDidDisappear__animated(let lhsAnimated), .m_viewDidDisappear__animated(let rhsAnimated)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsAnimated, rhs: rhsAnimated, with: matcher), lhsAnimated, rhsAnimated, "_ animated"))
				return Matcher.ComparisonResult(results)
            default: return .none
            }
        }

        func intValue() -> Int {
            switch self {
            case .m_fetchAlbumPhotos: return 0
            case .m_tapOnSettings: return 0
            case let .m_tapOnPhoto__photoID_photoID(p0): return p0.intValue
            case .m_viewDidLoad: return 0
            case let .m_viewWillAppear__animated(p0): return p0.intValue
            case let .m_viewDidAppear__animated(p0): return p0.intValue
            case let .m_viewWillDisappear__animated(p0): return p0.intValue
            case let .m_viewDidDisappear__animated(p0): return p0.intValue
            }
        }
        func assertionName() -> String {
            switch self {
            case .m_fetchAlbumPhotos: return ".fetchAlbumPhotos()"
            case .m_tapOnSettings: return ".tapOnSettings()"
            case .m_tapOnPhoto__photoID_photoID: return ".tapOnPhoto(photoID:)"
            case .m_viewDidLoad: return ".viewDidLoad()"
            case .m_viewWillAppear__animated: return ".viewWillAppear(_:)"
            case .m_viewDidAppear__animated: return ".viewDidAppear(_:)"
            case .m_viewWillDisappear__animated: return ".viewWillDisappear(_:)"
            case .m_viewDidDisappear__animated: return ".viewDidDisappear(_:)"
            }
        }
    }

    open class Given: StubbedMethod {
        fileprivate var method: MethodType

        private init(method: MethodType, products: [StubProduct]) {
            self.method = method
            super.init(products)
        }


    }

    public struct Verify {
        fileprivate var method: MethodType

        public static func fetchAlbumPhotos() -> Verify { return Verify(method: .m_fetchAlbumPhotos)}
        public static func tapOnSettings() -> Verify { return Verify(method: .m_tapOnSettings)}
        public static func tapOnPhoto(photoID: Parameter<String>) -> Verify { return Verify(method: .m_tapOnPhoto__photoID_photoID(`photoID`))}
        public static func viewDidLoad() -> Verify { return Verify(method: .m_viewDidLoad)}
        public static func viewWillAppear(_ animated: Parameter<Bool>) -> Verify { return Verify(method: .m_viewWillAppear__animated(`animated`))}
        public static func viewDidAppear(_ animated: Parameter<Bool>) -> Verify { return Verify(method: .m_viewDidAppear__animated(`animated`))}
        public static func viewWillDisappear(_ animated: Parameter<Bool>) -> Verify { return Verify(method: .m_viewWillDisappear__animated(`animated`))}
        public static func viewDidDisappear(_ animated: Parameter<Bool>) -> Verify { return Verify(method: .m_viewDidDisappear__animated(`animated`))}
    }

    public struct Perform {
        fileprivate var method: MethodType
        var performs: Any

        public static func fetchAlbumPhotos(perform: @escaping () -> Void) -> Perform {
            return Perform(method: .m_fetchAlbumPhotos, performs: perform)
        }
        public static func tapOnSettings(perform: @escaping () -> Void) -> Perform {
            return Perform(method: .m_tapOnSettings, performs: perform)
        }
        public static func tapOnPhoto(photoID: Parameter<String>, perform: @escaping (String) -> Void) -> Perform {
            return Perform(method: .m_tapOnPhoto__photoID_photoID(`photoID`), performs: perform)
        }
        public static func viewDidLoad(perform: @escaping () -> Void) -> Perform {
            return Perform(method: .m_viewDidLoad, performs: perform)
        }
        public static func viewWillAppear(_ animated: Parameter<Bool>, perform: @escaping (Bool) -> Void) -> Perform {
            return Perform(method: .m_viewWillAppear__animated(`animated`), performs: perform)
        }
        public static func viewDidAppear(_ animated: Parameter<Bool>, perform: @escaping (Bool) -> Void) -> Perform {
            return Perform(method: .m_viewDidAppear__animated(`animated`), performs: perform)
        }
        public static func viewWillDisappear(_ animated: Parameter<Bool>, perform: @escaping (Bool) -> Void) -> Perform {
            return Perform(method: .m_viewWillDisappear__animated(`animated`), performs: perform)
        }
        public static func viewDidDisappear(_ animated: Parameter<Bool>, perform: @escaping (Bool) -> Void) -> Perform {
            return Perform(method: .m_viewDidDisappear__animated(`animated`), performs: perform)
        }
    }

    public func given(_ method: Given) {
        methodReturnValues.append(method)
    }

    public func perform(_ method: Perform) {
        methodPerformValues.append(method)
        methodPerformValues.sort { $0.method.intValue() < $1.method.intValue() }
    }

    public func verify(_ method: Verify, count: Count = Count.moreOrEqual(to: 1), file: StaticString = #file, line: UInt = #line) {
        let fullMatches = matchingCalls(method, file: file, line: line)
        let success = count.matches(fullMatches)
        let assertionName = method.method.assertionName()
        let feedback: String = {
            guard !success else { return "" }
            return Utils.closestCallsMessage(
                for: self.invocations.map { invocation in
                    matcher.set(file: file, line: line)
                    defer { matcher.clearFileAndLine() }
                    return MethodType.compareParameters(lhs: invocation, rhs: method.method, matcher: matcher)
                },
                name: assertionName
            )
        }()
        MockyAssert(success, "Expected: \(count) invocations of `\(assertionName)`, but was: \(fullMatches).\(feedback)", file: file, line: line)
    }

    private func addInvocation(_ call: MethodType) {
        self.queue.sync { invocations.append(call) }
    }
    private func methodReturnValue(_ method: MethodType) throws -> StubProduct {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let candidates = sequencingPolicy.sorted(methodReturnValues, by: { $0.method.intValue() > $1.method.intValue() })
        let matched = candidates.first(where: { $0.isValid && MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch })
        guard let product = matched?.getProduct(policy: self.stubbingPolicy) else { throw MockError.notStubed }
        return product
    }
    private func methodPerformValue(_ method: MethodType) -> Any? {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let matched = methodPerformValues.reversed().first { MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch }
        return matched?.performs
    }
    private func matchingCalls(_ method: MethodType, file: StaticString?, line: UInt?) -> [MethodType] {
        matcher.set(file: file ?? self.file, line: line ?? self.line)
        defer { matcher.clearFileAndLine() }
        return invocations.filter { MethodType.compareParameters(lhs: $0, rhs: method, matcher: matcher).isFullMatch }
    }
    private func matchingCalls(_ method: Verify, file: StaticString?, line: UInt?) -> Int {
        return matchingCalls(method.method, file: file, line: line).count
    }
    private func givenGetterValue<T>(_ method: MethodType, _ message: String) -> T {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            onFatalFailure(message)
            Failure(message)
        }
    }
    private func optionalGivenGetterValue<T>(_ method: MethodType, _ message: String) -> T? {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            return nil
        }
    }
    private func onFatalFailure(_ message: String) {
        guard let file = self.file, let line = self.line else { return } // Let if fail if cannot handle gratefully
        SwiftyMockyTestObserver.handleFatalError(message: message, file: file, line: line)
    }
}

// MARK: - PhotosPresenterProtocol

open class PhotosPresenterProtocolMock: PhotosPresenterProtocol, Mock {
    public init(sequencing sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst, stubbing stubbingPolicy: StubbingPolicy = .wrap, file: StaticString = #file, line: UInt = #line) {
        SwiftyMockyTestObserver.setup()
        self.sequencingPolicy = sequencingPolicy
        self.stubbingPolicy = stubbingPolicy
        self.file = file
        self.line = line
    }

    var matcher: Matcher = Matcher.default
    var stubbingPolicy: StubbingPolicy = .wrap
    var sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst

    private var queue = DispatchQueue(label: "com.swiftymocky.invocations", qos: .userInteractive)
    private var invocations: [MethodType] = []
    private var methodReturnValues: [Given] = []
    private var methodPerformValues: [Perform] = []
    private var file: StaticString?
    private var line: UInt?

    public typealias PropertyStub = Given
    public typealias MethodStub = Given
    public typealias SubscriptStub = Given

    /// Convenience method - call setupMock() to extend debug information when failure occurs
    public func setupMock(file: StaticString = #file, line: UInt = #line) {
        self.file = file
        self.line = line
    }

    /// Clear mock internals. You can specify what to reset (invocations aka verify, givens or performs) or leave it empty to clear all mock internals
    public func resetMock(_ scopes: MockScope...) {
        let scopes: [MockScope] = scopes.isEmpty ? [.invocation, .given, .perform] : scopes
        if scopes.contains(.invocation) { invocations = [] }
        if scopes.contains(.given) { methodReturnValues = [] }
        if scopes.contains(.perform) { methodPerformValues = [] }
    }





    open func presentStartLoading() {
        addInvocation(.m_presentStartLoading)
		let perform = methodPerformValue(.m_presentStartLoading) as? () -> Void
		perform?()
    }

    open func presentStopLoading() {
        addInvocation(.m_presentStopLoading)
		let perform = methodPerformValue(.m_presentStopLoading) as? () -> Void
		perform?()
    }

    open func presentPhotos(photos: [Media]) {
        addInvocation(.m_presentPhotos__photos_photos(Parameter<[Media]>.value(`photos`)))
		let perform = methodPerformValue(.m_presentPhotos__photos_photos(Parameter<[Media]>.value(`photos`))) as? ([Media]) -> Void
		perform?(`photos`)
    }

    open func handle(error: KiliaroError?) {
        addInvocation(.m_handle__error_error(Parameter<KiliaroError?>.value(`error`)))
		let perform = methodPerformValue(.m_handle__error_error(Parameter<KiliaroError?>.value(`error`))) as? (KiliaroError?) -> Void
		perform?(`error`)
    }

    open func handle(error: KiliaroError?, dismiss: (() -> Void)?) {
        addInvocation(.m_handle__error_errordismiss_dismiss(Parameter<KiliaroError?>.value(`error`), Parameter<(() -> Void)?>.value(`dismiss`)))
		let perform = methodPerformValue(.m_handle__error_errordismiss_dismiss(Parameter<KiliaroError?>.value(`error`), Parameter<(() -> Void)?>.value(`dismiss`))) as? (KiliaroError?, (() -> Void)?) -> Void
		perform?(`error`, `dismiss`)
    }


    fileprivate enum MethodType {
        case m_presentStartLoading
        case m_presentStopLoading
        case m_presentPhotos__photos_photos(Parameter<[Media]>)
        case m_handle__error_error(Parameter<KiliaroError?>)
        case m_handle__error_errordismiss_dismiss(Parameter<KiliaroError?>, Parameter<(() -> Void)?>)

        static func compareParameters(lhs: MethodType, rhs: MethodType, matcher: Matcher) -> Matcher.ComparisonResult {
            switch (lhs, rhs) {
            case (.m_presentStartLoading, .m_presentStartLoading): return .match

            case (.m_presentStopLoading, .m_presentStopLoading): return .match

            case (.m_presentPhotos__photos_photos(let lhsPhotos), .m_presentPhotos__photos_photos(let rhsPhotos)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsPhotos, rhs: rhsPhotos, with: matcher), lhsPhotos, rhsPhotos, "photos"))
				return Matcher.ComparisonResult(results)

            case (.m_handle__error_error(let lhsError), .m_handle__error_error(let rhsError)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsError, rhs: rhsError, with: matcher), lhsError, rhsError, "error"))
				return Matcher.ComparisonResult(results)

            case (.m_handle__error_errordismiss_dismiss(let lhsError, let lhsDismiss), .m_handle__error_errordismiss_dismiss(let rhsError, let rhsDismiss)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsError, rhs: rhsError, with: matcher), lhsError, rhsError, "error"))
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsDismiss, rhs: rhsDismiss, with: matcher), lhsDismiss, rhsDismiss, "dismiss"))
				return Matcher.ComparisonResult(results)
            default: return .none
            }
        }

        func intValue() -> Int {
            switch self {
            case .m_presentStartLoading: return 0
            case .m_presentStopLoading: return 0
            case let .m_presentPhotos__photos_photos(p0): return p0.intValue
            case let .m_handle__error_error(p0): return p0.intValue
            case let .m_handle__error_errordismiss_dismiss(p0, p1): return p0.intValue + p1.intValue
            }
        }
        func assertionName() -> String {
            switch self {
            case .m_presentStartLoading: return ".presentStartLoading()"
            case .m_presentStopLoading: return ".presentStopLoading()"
            case .m_presentPhotos__photos_photos: return ".presentPhotos(photos:)"
            case .m_handle__error_error: return ".handle(error:)"
            case .m_handle__error_errordismiss_dismiss: return ".handle(error:dismiss:)"
            }
        }
    }

    open class Given: StubbedMethod {
        fileprivate var method: MethodType

        private init(method: MethodType, products: [StubProduct]) {
            self.method = method
            super.init(products)
        }


    }

    public struct Verify {
        fileprivate var method: MethodType

        public static func presentStartLoading() -> Verify { return Verify(method: .m_presentStartLoading)}
        public static func presentStopLoading() -> Verify { return Verify(method: .m_presentStopLoading)}
        public static func presentPhotos(photos: Parameter<[Media]>) -> Verify { return Verify(method: .m_presentPhotos__photos_photos(`photos`))}
        public static func handle(error: Parameter<KiliaroError?>) -> Verify { return Verify(method: .m_handle__error_error(`error`))}
        public static func handle(error: Parameter<KiliaroError?>, dismiss: Parameter<(() -> Void)?>) -> Verify { return Verify(method: .m_handle__error_errordismiss_dismiss(`error`, `dismiss`))}
    }

    public struct Perform {
        fileprivate var method: MethodType
        var performs: Any

        public static func presentStartLoading(perform: @escaping () -> Void) -> Perform {
            return Perform(method: .m_presentStartLoading, performs: perform)
        }
        public static func presentStopLoading(perform: @escaping () -> Void) -> Perform {
            return Perform(method: .m_presentStopLoading, performs: perform)
        }
        public static func presentPhotos(photos: Parameter<[Media]>, perform: @escaping ([Media]) -> Void) -> Perform {
            return Perform(method: .m_presentPhotos__photos_photos(`photos`), performs: perform)
        }
        public static func handle(error: Parameter<KiliaroError?>, perform: @escaping (KiliaroError?) -> Void) -> Perform {
            return Perform(method: .m_handle__error_error(`error`), performs: perform)
        }
        public static func handle(error: Parameter<KiliaroError?>, dismiss: Parameter<(() -> Void)?>, perform: @escaping (KiliaroError?, (() -> Void)?) -> Void) -> Perform {
            return Perform(method: .m_handle__error_errordismiss_dismiss(`error`, `dismiss`), performs: perform)
        }
    }

    public func given(_ method: Given) {
        methodReturnValues.append(method)
    }

    public func perform(_ method: Perform) {
        methodPerformValues.append(method)
        methodPerformValues.sort { $0.method.intValue() < $1.method.intValue() }
    }

    public func verify(_ method: Verify, count: Count = Count.moreOrEqual(to: 1), file: StaticString = #file, line: UInt = #line) {
        let fullMatches = matchingCalls(method, file: file, line: line)
        let success = count.matches(fullMatches)
        let assertionName = method.method.assertionName()
        let feedback: String = {
            guard !success else { return "" }
            return Utils.closestCallsMessage(
                for: self.invocations.map { invocation in
                    matcher.set(file: file, line: line)
                    defer { matcher.clearFileAndLine() }
                    return MethodType.compareParameters(lhs: invocation, rhs: method.method, matcher: matcher)
                },
                name: assertionName
            )
        }()
        MockyAssert(success, "Expected: \(count) invocations of `\(assertionName)`, but was: \(fullMatches).\(feedback)", file: file, line: line)
    }

    private func addInvocation(_ call: MethodType) {
        self.queue.sync { invocations.append(call) }
    }
    private func methodReturnValue(_ method: MethodType) throws -> StubProduct {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let candidates = sequencingPolicy.sorted(methodReturnValues, by: { $0.method.intValue() > $1.method.intValue() })
        let matched = candidates.first(where: { $0.isValid && MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch })
        guard let product = matched?.getProduct(policy: self.stubbingPolicy) else { throw MockError.notStubed }
        return product
    }
    private func methodPerformValue(_ method: MethodType) -> Any? {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let matched = methodPerformValues.reversed().first { MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch }
        return matched?.performs
    }
    private func matchingCalls(_ method: MethodType, file: StaticString?, line: UInt?) -> [MethodType] {
        matcher.set(file: file ?? self.file, line: line ?? self.line)
        defer { matcher.clearFileAndLine() }
        return invocations.filter { MethodType.compareParameters(lhs: $0, rhs: method, matcher: matcher).isFullMatch }
    }
    private func matchingCalls(_ method: Verify, file: StaticString?, line: UInt?) -> Int {
        return matchingCalls(method.method, file: file, line: line).count
    }
    private func givenGetterValue<T>(_ method: MethodType, _ message: String) -> T {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            onFatalFailure(message)
            Failure(message)
        }
    }
    private func optionalGivenGetterValue<T>(_ method: MethodType, _ message: String) -> T? {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            return nil
        }
    }
    private func onFatalFailure(_ message: String) {
        guard let file = self.file, let line = self.line else { return } // Let if fail if cannot handle gratefully
        SwiftyMockyTestObserver.handleFatalError(message: message, file: file, line: line)
    }
}

// MARK: - PhotosRouterProtocol

open class PhotosRouterProtocolMock: PhotosRouterProtocol, Mock {
    public init(sequencing sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst, stubbing stubbingPolicy: StubbingPolicy = .wrap, file: StaticString = #file, line: UInt = #line) {
        SwiftyMockyTestObserver.setup()
        self.sequencingPolicy = sequencingPolicy
        self.stubbingPolicy = stubbingPolicy
        self.file = file
        self.line = line
    }

    var matcher: Matcher = Matcher.default
    var stubbingPolicy: StubbingPolicy = .wrap
    var sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst

    private var queue = DispatchQueue(label: "com.swiftymocky.invocations", qos: .userInteractive)
    private var invocations: [MethodType] = []
    private var methodReturnValues: [Given] = []
    private var methodPerformValues: [Perform] = []
    private var file: StaticString?
    private var line: UInt?

    public typealias PropertyStub = Given
    public typealias MethodStub = Given
    public typealias SubscriptStub = Given

    /// Convenience method - call setupMock() to extend debug information when failure occurs
    public func setupMock(file: StaticString = #file, line: UInt = #line) {
        self.file = file
        self.line = line
    }

    /// Clear mock internals. You can specify what to reset (invocations aka verify, givens or performs) or leave it empty to clear all mock internals
    public func resetMock(_ scopes: MockScope...) {
        let scopes: [MockScope] = scopes.isEmpty ? [.invocation, .given, .perform] : scopes
        if scopes.contains(.invocation) { invocations = [] }
        if scopes.contains(.given) { methodReturnValues = [] }
        if scopes.contains(.perform) { methodPerformValues = [] }
    }





    open func navigateToPhotoDetail(photo: Media) {
        addInvocation(.m_navigateToPhotoDetail__photo_photo(Parameter<Media>.value(`photo`)))
		let perform = methodPerformValue(.m_navigateToPhotoDetail__photo_photo(Parameter<Media>.value(`photo`))) as? (Media) -> Void
		perform?(`photo`)
    }

    open func navigateToSettings() {
        addInvocation(.m_navigateToSettings)
		let perform = methodPerformValue(.m_navigateToSettings) as? () -> Void
		perform?()
    }

    open func dismiss() {
        addInvocation(.m_dismiss)
		let perform = methodPerformValue(.m_dismiss) as? () -> Void
		perform?()
    }


    fileprivate enum MethodType {
        case m_navigateToPhotoDetail__photo_photo(Parameter<Media>)
        case m_navigateToSettings
        case m_dismiss

        static func compareParameters(lhs: MethodType, rhs: MethodType, matcher: Matcher) -> Matcher.ComparisonResult {
            switch (lhs, rhs) {
            case (.m_navigateToPhotoDetail__photo_photo(let lhsPhoto), .m_navigateToPhotoDetail__photo_photo(let rhsPhoto)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsPhoto, rhs: rhsPhoto, with: matcher), lhsPhoto, rhsPhoto, "photo"))
				return Matcher.ComparisonResult(results)

            case (.m_navigateToSettings, .m_navigateToSettings): return .match

            case (.m_dismiss, .m_dismiss): return .match
            default: return .none
            }
        }

        func intValue() -> Int {
            switch self {
            case let .m_navigateToPhotoDetail__photo_photo(p0): return p0.intValue
            case .m_navigateToSettings: return 0
            case .m_dismiss: return 0
            }
        }
        func assertionName() -> String {
            switch self {
            case .m_navigateToPhotoDetail__photo_photo: return ".navigateToPhotoDetail(photo:)"
            case .m_navigateToSettings: return ".navigateToSettings()"
            case .m_dismiss: return ".dismiss()"
            }
        }
    }

    open class Given: StubbedMethod {
        fileprivate var method: MethodType

        private init(method: MethodType, products: [StubProduct]) {
            self.method = method
            super.init(products)
        }


    }

    public struct Verify {
        fileprivate var method: MethodType

        public static func navigateToPhotoDetail(photo: Parameter<Media>) -> Verify { return Verify(method: .m_navigateToPhotoDetail__photo_photo(`photo`))}
        public static func navigateToSettings() -> Verify { return Verify(method: .m_navigateToSettings)}
        public static func dismiss() -> Verify { return Verify(method: .m_dismiss)}
    }

    public struct Perform {
        fileprivate var method: MethodType
        var performs: Any

        public static func navigateToPhotoDetail(photo: Parameter<Media>, perform: @escaping (Media) -> Void) -> Perform {
            return Perform(method: .m_navigateToPhotoDetail__photo_photo(`photo`), performs: perform)
        }
        public static func navigateToSettings(perform: @escaping () -> Void) -> Perform {
            return Perform(method: .m_navigateToSettings, performs: perform)
        }
        public static func dismiss(perform: @escaping () -> Void) -> Perform {
            return Perform(method: .m_dismiss, performs: perform)
        }
    }

    public func given(_ method: Given) {
        methodReturnValues.append(method)
    }

    public func perform(_ method: Perform) {
        methodPerformValues.append(method)
        methodPerformValues.sort { $0.method.intValue() < $1.method.intValue() }
    }

    public func verify(_ method: Verify, count: Count = Count.moreOrEqual(to: 1), file: StaticString = #file, line: UInt = #line) {
        let fullMatches = matchingCalls(method, file: file, line: line)
        let success = count.matches(fullMatches)
        let assertionName = method.method.assertionName()
        let feedback: String = {
            guard !success else { return "" }
            return Utils.closestCallsMessage(
                for: self.invocations.map { invocation in
                    matcher.set(file: file, line: line)
                    defer { matcher.clearFileAndLine() }
                    return MethodType.compareParameters(lhs: invocation, rhs: method.method, matcher: matcher)
                },
                name: assertionName
            )
        }()
        MockyAssert(success, "Expected: \(count) invocations of `\(assertionName)`, but was: \(fullMatches).\(feedback)", file: file, line: line)
    }

    private func addInvocation(_ call: MethodType) {
        self.queue.sync { invocations.append(call) }
    }
    private func methodReturnValue(_ method: MethodType) throws -> StubProduct {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let candidates = sequencingPolicy.sorted(methodReturnValues, by: { $0.method.intValue() > $1.method.intValue() })
        let matched = candidates.first(where: { $0.isValid && MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch })
        guard let product = matched?.getProduct(policy: self.stubbingPolicy) else { throw MockError.notStubed }
        return product
    }
    private func methodPerformValue(_ method: MethodType) -> Any? {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let matched = methodPerformValues.reversed().first { MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch }
        return matched?.performs
    }
    private func matchingCalls(_ method: MethodType, file: StaticString?, line: UInt?) -> [MethodType] {
        matcher.set(file: file ?? self.file, line: line ?? self.line)
        defer { matcher.clearFileAndLine() }
        return invocations.filter { MethodType.compareParameters(lhs: $0, rhs: method, matcher: matcher).isFullMatch }
    }
    private func matchingCalls(_ method: Verify, file: StaticString?, line: UInt?) -> Int {
        return matchingCalls(method.method, file: file, line: line).count
    }
    private func givenGetterValue<T>(_ method: MethodType, _ message: String) -> T {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            onFatalFailure(message)
            Failure(message)
        }
    }
    private func optionalGivenGetterValue<T>(_ method: MethodType, _ message: String) -> T? {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            return nil
        }
    }
    private func onFatalFailure(_ message: String) {
        guard let file = self.file, let line = self.line else { return } // Let if fail if cannot handle gratefully
        SwiftyMockyTestObserver.handleFatalError(message: message, file: file, line: line)
    }
}

// MARK: - PhotosViewControllerProtocol

open class PhotosViewControllerProtocolMock: PhotosViewControllerProtocol, Mock {
    public init(sequencing sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst, stubbing stubbingPolicy: StubbingPolicy = .wrap, file: StaticString = #file, line: UInt = #line) {
        SwiftyMockyTestObserver.setup()
        self.sequencingPolicy = sequencingPolicy
        self.stubbingPolicy = stubbingPolicy
        self.file = file
        self.line = line
    }

    var matcher: Matcher = Matcher.default
    var stubbingPolicy: StubbingPolicy = .wrap
    var sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst

    private var queue = DispatchQueue(label: "com.swiftymocky.invocations", qos: .userInteractive)
    private var invocations: [MethodType] = []
    private var methodReturnValues: [Given] = []
    private var methodPerformValues: [Perform] = []
    private var file: StaticString?
    private var line: UInt?

    public typealias PropertyStub = Given
    public typealias MethodStub = Given
    public typealias SubscriptStub = Given

    /// Convenience method - call setupMock() to extend debug information when failure occurs
    public func setupMock(file: StaticString = #file, line: UInt = #line) {
        self.file = file
        self.line = line
    }

    /// Clear mock internals. You can specify what to reset (invocations aka verify, givens or performs) or leave it empty to clear all mock internals
    public func resetMock(_ scopes: MockScope...) {
        let scopes: [MockScope] = scopes.isEmpty ? [.invocation, .given, .perform] : scopes
        if scopes.contains(.invocation) { invocations = [] }
        if scopes.contains(.given) { methodReturnValues = [] }
        if scopes.contains(.perform) { methodPerformValues = [] }
    }

    public var viewController: KiliaroSceneViewController! {
		get {	invocations.append(.p_viewController_get); return __p_viewController ?? optionalGivenGetterValue(.p_viewController_get, "PhotosViewControllerProtocolMock - stub value for viewController was not defined") }
	}
	private var __p_viewController: (KiliaroSceneViewController)?





    open func showError(_ error: KiliaroError?) {
        addInvocation(.m_showError__error(Parameter<KiliaroError?>.value(`error`)))
		let perform = methodPerformValue(.m_showError__error(Parameter<KiliaroError?>.value(`error`))) as? (KiliaroError?) -> Void
		perform?(`error`)
    }

    open func showStartLoading() {
        addInvocation(.m_showStartLoading)
		let perform = methodPerformValue(.m_showStartLoading) as? () -> Void
		perform?()
    }

    open func showStopLoading() {
        addInvocation(.m_showStopLoading)
		let perform = methodPerformValue(.m_showStopLoading) as? () -> Void
		perform?()
    }

    open func showPhotos(photos: [PhotoViewModel]) {
        addInvocation(.m_showPhotos__photos_photos(Parameter<[PhotoViewModel]>.value(`photos`)))
		let perform = methodPerformValue(.m_showPhotos__photos_photos(Parameter<[PhotoViewModel]>.value(`photos`))) as? ([PhotoViewModel]) -> Void
		perform?(`photos`)
    }


    fileprivate enum MethodType {
        case m_showError__error(Parameter<KiliaroError?>)
        case m_showStartLoading
        case m_showStopLoading
        case m_showPhotos__photos_photos(Parameter<[PhotoViewModel]>)
        case p_viewController_get

        static func compareParameters(lhs: MethodType, rhs: MethodType, matcher: Matcher) -> Matcher.ComparisonResult {
            switch (lhs, rhs) {
            case (.m_showError__error(let lhsError), .m_showError__error(let rhsError)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsError, rhs: rhsError, with: matcher), lhsError, rhsError, "_ error"))
				return Matcher.ComparisonResult(results)

            case (.m_showStartLoading, .m_showStartLoading): return .match

            case (.m_showStopLoading, .m_showStopLoading): return .match

            case (.m_showPhotos__photos_photos(let lhsPhotos), .m_showPhotos__photos_photos(let rhsPhotos)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsPhotos, rhs: rhsPhotos, with: matcher), lhsPhotos, rhsPhotos, "photos"))
				return Matcher.ComparisonResult(results)
            case (.p_viewController_get,.p_viewController_get): return Matcher.ComparisonResult.match
            default: return .none
            }
        }

        func intValue() -> Int {
            switch self {
            case let .m_showError__error(p0): return p0.intValue
            case .m_showStartLoading: return 0
            case .m_showStopLoading: return 0
            case let .m_showPhotos__photos_photos(p0): return p0.intValue
            case .p_viewController_get: return 0
            }
        }
        func assertionName() -> String {
            switch self {
            case .m_showError__error: return ".showError(_:)"
            case .m_showStartLoading: return ".showStartLoading()"
            case .m_showStopLoading: return ".showStopLoading()"
            case .m_showPhotos__photos_photos: return ".showPhotos(photos:)"
            case .p_viewController_get: return "[get] .viewController"
            }
        }
    }

    open class Given: StubbedMethod {
        fileprivate var method: MethodType

        private init(method: MethodType, products: [StubProduct]) {
            self.method = method
            super.init(products)
        }

        public static func viewController(getter defaultValue: KiliaroSceneViewController?...) -> PropertyStub {
            return Given(method: .p_viewController_get, products: defaultValue.map({ StubProduct.return($0 as Any) }))
        }

    }

    public struct Verify {
        fileprivate var method: MethodType

        public static func showError(_ error: Parameter<KiliaroError?>) -> Verify { return Verify(method: .m_showError__error(`error`))}
        public static func showStartLoading() -> Verify { return Verify(method: .m_showStartLoading)}
        public static func showStopLoading() -> Verify { return Verify(method: .m_showStopLoading)}
        public static func showPhotos(photos: Parameter<[PhotoViewModel]>) -> Verify { return Verify(method: .m_showPhotos__photos_photos(`photos`))}
        public static var viewController: Verify { return Verify(method: .p_viewController_get) }
    }

    public struct Perform {
        fileprivate var method: MethodType
        var performs: Any

        public static func showError(_ error: Parameter<KiliaroError?>, perform: @escaping (KiliaroError?) -> Void) -> Perform {
            return Perform(method: .m_showError__error(`error`), performs: perform)
        }
        public static func showStartLoading(perform: @escaping () -> Void) -> Perform {
            return Perform(method: .m_showStartLoading, performs: perform)
        }
        public static func showStopLoading(perform: @escaping () -> Void) -> Perform {
            return Perform(method: .m_showStopLoading, performs: perform)
        }
        public static func showPhotos(photos: Parameter<[PhotoViewModel]>, perform: @escaping ([PhotoViewModel]) -> Void) -> Perform {
            return Perform(method: .m_showPhotos__photos_photos(`photos`), performs: perform)
        }
    }

    public func given(_ method: Given) {
        methodReturnValues.append(method)
    }

    public func perform(_ method: Perform) {
        methodPerformValues.append(method)
        methodPerformValues.sort { $0.method.intValue() < $1.method.intValue() }
    }

    public func verify(_ method: Verify, count: Count = Count.moreOrEqual(to: 1), file: StaticString = #file, line: UInt = #line) {
        let fullMatches = matchingCalls(method, file: file, line: line)
        let success = count.matches(fullMatches)
        let assertionName = method.method.assertionName()
        let feedback: String = {
            guard !success else { return "" }
            return Utils.closestCallsMessage(
                for: self.invocations.map { invocation in
                    matcher.set(file: file, line: line)
                    defer { matcher.clearFileAndLine() }
                    return MethodType.compareParameters(lhs: invocation, rhs: method.method, matcher: matcher)
                },
                name: assertionName
            )
        }()
        MockyAssert(success, "Expected: \(count) invocations of `\(assertionName)`, but was: \(fullMatches).\(feedback)", file: file, line: line)
    }

    private func addInvocation(_ call: MethodType) {
        self.queue.sync { invocations.append(call) }
    }
    private func methodReturnValue(_ method: MethodType) throws -> StubProduct {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let candidates = sequencingPolicy.sorted(methodReturnValues, by: { $0.method.intValue() > $1.method.intValue() })
        let matched = candidates.first(where: { $0.isValid && MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch })
        guard let product = matched?.getProduct(policy: self.stubbingPolicy) else { throw MockError.notStubed }
        return product
    }
    private func methodPerformValue(_ method: MethodType) -> Any? {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let matched = methodPerformValues.reversed().first { MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch }
        return matched?.performs
    }
    private func matchingCalls(_ method: MethodType, file: StaticString?, line: UInt?) -> [MethodType] {
        matcher.set(file: file ?? self.file, line: line ?? self.line)
        defer { matcher.clearFileAndLine() }
        return invocations.filter { MethodType.compareParameters(lhs: $0, rhs: method, matcher: matcher).isFullMatch }
    }
    private func matchingCalls(_ method: Verify, file: StaticString?, line: UInt?) -> Int {
        return matchingCalls(method.method, file: file, line: line).count
    }
    private func givenGetterValue<T>(_ method: MethodType, _ message: String) -> T {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            onFatalFailure(message)
            Failure(message)
        }
    }
    private func optionalGivenGetterValue<T>(_ method: MethodType, _ message: String) -> T? {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            return nil
        }
    }
    private func onFatalFailure(_ message: String) {
        guard let file = self.file, let line = self.line else { return } // Let if fail if cannot handle gratefully
        SwiftyMockyTestObserver.handleFatalError(message: message, file: file, line: line)
    }
}

// MARK: - SettingsInteractorProtocol

open class SettingsInteractorProtocolMock: SettingsInteractorProtocol, Mock {
    public init(sequencing sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst, stubbing stubbingPolicy: StubbingPolicy = .wrap, file: StaticString = #file, line: UInt = #line) {
        SwiftyMockyTestObserver.setup()
        self.sequencingPolicy = sequencingPolicy
        self.stubbingPolicy = stubbingPolicy
        self.file = file
        self.line = line
    }

    var matcher: Matcher = Matcher.default
    var stubbingPolicy: StubbingPolicy = .wrap
    var sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst

    private var queue = DispatchQueue(label: "com.swiftymocky.invocations", qos: .userInteractive)
    private var invocations: [MethodType] = []
    private var methodReturnValues: [Given] = []
    private var methodPerformValues: [Perform] = []
    private var file: StaticString?
    private var line: UInt?

    public typealias PropertyStub = Given
    public typealias MethodStub = Given
    public typealias SubscriptStub = Given

    /// Convenience method - call setupMock() to extend debug information when failure occurs
    public func setupMock(file: StaticString = #file, line: UInt = #line) {
        self.file = file
        self.line = line
    }

    /// Clear mock internals. You can specify what to reset (invocations aka verify, givens or performs) or leave it empty to clear all mock internals
    public func resetMock(_ scopes: MockScope...) {
        let scopes: [MockScope] = scopes.isEmpty ? [.invocation, .given, .perform] : scopes
        if scopes.contains(.invocation) { invocations = [] }
        if scopes.contains(.given) { methodReturnValues = [] }
        if scopes.contains(.perform) { methodPerformValues = [] }
    }





    open func didTapOnSubmit() {
        addInvocation(.m_didTapOnSubmit)
		let perform = methodPerformValue(.m_didTapOnSubmit) as? () -> Void
		perform?()
    }

    open func viewDidLoad() {
        addInvocation(.m_viewDidLoad)
		let perform = methodPerformValue(.m_viewDidLoad) as? () -> Void
		perform?()
    }

    open func viewWillAppear(_ animated: Bool) {
        addInvocation(.m_viewWillAppear__animated(Parameter<Bool>.value(`animated`)))
		let perform = methodPerformValue(.m_viewWillAppear__animated(Parameter<Bool>.value(`animated`))) as? (Bool) -> Void
		perform?(`animated`)
    }

    open func viewDidAppear(_ animated: Bool) {
        addInvocation(.m_viewDidAppear__animated(Parameter<Bool>.value(`animated`)))
		let perform = methodPerformValue(.m_viewDidAppear__animated(Parameter<Bool>.value(`animated`))) as? (Bool) -> Void
		perform?(`animated`)
    }

    open func viewWillDisappear(_ animated: Bool) {
        addInvocation(.m_viewWillDisappear__animated(Parameter<Bool>.value(`animated`)))
		let perform = methodPerformValue(.m_viewWillDisappear__animated(Parameter<Bool>.value(`animated`))) as? (Bool) -> Void
		perform?(`animated`)
    }

    open func viewDidDisappear(_ animated: Bool) {
        addInvocation(.m_viewDidDisappear__animated(Parameter<Bool>.value(`animated`)))
		let perform = methodPerformValue(.m_viewDidDisappear__animated(Parameter<Bool>.value(`animated`))) as? (Bool) -> Void
		perform?(`animated`)
    }


    fileprivate enum MethodType {
        case m_didTapOnSubmit
        case m_viewDidLoad
        case m_viewWillAppear__animated(Parameter<Bool>)
        case m_viewDidAppear__animated(Parameter<Bool>)
        case m_viewWillDisappear__animated(Parameter<Bool>)
        case m_viewDidDisappear__animated(Parameter<Bool>)

        static func compareParameters(lhs: MethodType, rhs: MethodType, matcher: Matcher) -> Matcher.ComparisonResult {
            switch (lhs, rhs) {
            case (.m_didTapOnSubmit, .m_didTapOnSubmit): return .match

            case (.m_viewDidLoad, .m_viewDidLoad): return .match

            case (.m_viewWillAppear__animated(let lhsAnimated), .m_viewWillAppear__animated(let rhsAnimated)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsAnimated, rhs: rhsAnimated, with: matcher), lhsAnimated, rhsAnimated, "_ animated"))
				return Matcher.ComparisonResult(results)

            case (.m_viewDidAppear__animated(let lhsAnimated), .m_viewDidAppear__animated(let rhsAnimated)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsAnimated, rhs: rhsAnimated, with: matcher), lhsAnimated, rhsAnimated, "_ animated"))
				return Matcher.ComparisonResult(results)

            case (.m_viewWillDisappear__animated(let lhsAnimated), .m_viewWillDisappear__animated(let rhsAnimated)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsAnimated, rhs: rhsAnimated, with: matcher), lhsAnimated, rhsAnimated, "_ animated"))
				return Matcher.ComparisonResult(results)

            case (.m_viewDidDisappear__animated(let lhsAnimated), .m_viewDidDisappear__animated(let rhsAnimated)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsAnimated, rhs: rhsAnimated, with: matcher), lhsAnimated, rhsAnimated, "_ animated"))
				return Matcher.ComparisonResult(results)
            default: return .none
            }
        }

        func intValue() -> Int {
            switch self {
            case .m_didTapOnSubmit: return 0
            case .m_viewDidLoad: return 0
            case let .m_viewWillAppear__animated(p0): return p0.intValue
            case let .m_viewDidAppear__animated(p0): return p0.intValue
            case let .m_viewWillDisappear__animated(p0): return p0.intValue
            case let .m_viewDidDisappear__animated(p0): return p0.intValue
            }
        }
        func assertionName() -> String {
            switch self {
            case .m_didTapOnSubmit: return ".didTapOnSubmit()"
            case .m_viewDidLoad: return ".viewDidLoad()"
            case .m_viewWillAppear__animated: return ".viewWillAppear(_:)"
            case .m_viewDidAppear__animated: return ".viewDidAppear(_:)"
            case .m_viewWillDisappear__animated: return ".viewWillDisappear(_:)"
            case .m_viewDidDisappear__animated: return ".viewDidDisappear(_:)"
            }
        }
    }

    open class Given: StubbedMethod {
        fileprivate var method: MethodType

        private init(method: MethodType, products: [StubProduct]) {
            self.method = method
            super.init(products)
        }


    }

    public struct Verify {
        fileprivate var method: MethodType

        public static func didTapOnSubmit() -> Verify { return Verify(method: .m_didTapOnSubmit)}
        public static func viewDidLoad() -> Verify { return Verify(method: .m_viewDidLoad)}
        public static func viewWillAppear(_ animated: Parameter<Bool>) -> Verify { return Verify(method: .m_viewWillAppear__animated(`animated`))}
        public static func viewDidAppear(_ animated: Parameter<Bool>) -> Verify { return Verify(method: .m_viewDidAppear__animated(`animated`))}
        public static func viewWillDisappear(_ animated: Parameter<Bool>) -> Verify { return Verify(method: .m_viewWillDisappear__animated(`animated`))}
        public static func viewDidDisappear(_ animated: Parameter<Bool>) -> Verify { return Verify(method: .m_viewDidDisappear__animated(`animated`))}
    }

    public struct Perform {
        fileprivate var method: MethodType
        var performs: Any

        public static func didTapOnSubmit(perform: @escaping () -> Void) -> Perform {
            return Perform(method: .m_didTapOnSubmit, performs: perform)
        }
        public static func viewDidLoad(perform: @escaping () -> Void) -> Perform {
            return Perform(method: .m_viewDidLoad, performs: perform)
        }
        public static func viewWillAppear(_ animated: Parameter<Bool>, perform: @escaping (Bool) -> Void) -> Perform {
            return Perform(method: .m_viewWillAppear__animated(`animated`), performs: perform)
        }
        public static func viewDidAppear(_ animated: Parameter<Bool>, perform: @escaping (Bool) -> Void) -> Perform {
            return Perform(method: .m_viewDidAppear__animated(`animated`), performs: perform)
        }
        public static func viewWillDisappear(_ animated: Parameter<Bool>, perform: @escaping (Bool) -> Void) -> Perform {
            return Perform(method: .m_viewWillDisappear__animated(`animated`), performs: perform)
        }
        public static func viewDidDisappear(_ animated: Parameter<Bool>, perform: @escaping (Bool) -> Void) -> Perform {
            return Perform(method: .m_viewDidDisappear__animated(`animated`), performs: perform)
        }
    }

    public func given(_ method: Given) {
        methodReturnValues.append(method)
    }

    public func perform(_ method: Perform) {
        methodPerformValues.append(method)
        methodPerformValues.sort { $0.method.intValue() < $1.method.intValue() }
    }

    public func verify(_ method: Verify, count: Count = Count.moreOrEqual(to: 1), file: StaticString = #file, line: UInt = #line) {
        let fullMatches = matchingCalls(method, file: file, line: line)
        let success = count.matches(fullMatches)
        let assertionName = method.method.assertionName()
        let feedback: String = {
            guard !success else { return "" }
            return Utils.closestCallsMessage(
                for: self.invocations.map { invocation in
                    matcher.set(file: file, line: line)
                    defer { matcher.clearFileAndLine() }
                    return MethodType.compareParameters(lhs: invocation, rhs: method.method, matcher: matcher)
                },
                name: assertionName
            )
        }()
        MockyAssert(success, "Expected: \(count) invocations of `\(assertionName)`, but was: \(fullMatches).\(feedback)", file: file, line: line)
    }

    private func addInvocation(_ call: MethodType) {
        self.queue.sync { invocations.append(call) }
    }
    private func methodReturnValue(_ method: MethodType) throws -> StubProduct {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let candidates = sequencingPolicy.sorted(methodReturnValues, by: { $0.method.intValue() > $1.method.intValue() })
        let matched = candidates.first(where: { $0.isValid && MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch })
        guard let product = matched?.getProduct(policy: self.stubbingPolicy) else { throw MockError.notStubed }
        return product
    }
    private func methodPerformValue(_ method: MethodType) -> Any? {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let matched = methodPerformValues.reversed().first { MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch }
        return matched?.performs
    }
    private func matchingCalls(_ method: MethodType, file: StaticString?, line: UInt?) -> [MethodType] {
        matcher.set(file: file ?? self.file, line: line ?? self.line)
        defer { matcher.clearFileAndLine() }
        return invocations.filter { MethodType.compareParameters(lhs: $0, rhs: method, matcher: matcher).isFullMatch }
    }
    private func matchingCalls(_ method: Verify, file: StaticString?, line: UInt?) -> Int {
        return matchingCalls(method.method, file: file, line: line).count
    }
    private func givenGetterValue<T>(_ method: MethodType, _ message: String) -> T {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            onFatalFailure(message)
            Failure(message)
        }
    }
    private func optionalGivenGetterValue<T>(_ method: MethodType, _ message: String) -> T? {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            return nil
        }
    }
    private func onFatalFailure(_ message: String) {
        guard let file = self.file, let line = self.line else { return } // Let if fail if cannot handle gratefully
        SwiftyMockyTestObserver.handleFatalError(message: message, file: file, line: line)
    }
}

// MARK: - SettingsPresenterProtocol

open class SettingsPresenterProtocolMock: SettingsPresenterProtocol, Mock {
    public init(sequencing sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst, stubbing stubbingPolicy: StubbingPolicy = .wrap, file: StaticString = #file, line: UInt = #line) {
        SwiftyMockyTestObserver.setup()
        self.sequencingPolicy = sequencingPolicy
        self.stubbingPolicy = stubbingPolicy
        self.file = file
        self.line = line
    }

    var matcher: Matcher = Matcher.default
    var stubbingPolicy: StubbingPolicy = .wrap
    var sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst

    private var queue = DispatchQueue(label: "com.swiftymocky.invocations", qos: .userInteractive)
    private var invocations: [MethodType] = []
    private var methodReturnValues: [Given] = []
    private var methodPerformValues: [Perform] = []
    private var file: StaticString?
    private var line: UInt?

    public typealias PropertyStub = Given
    public typealias MethodStub = Given
    public typealias SubscriptStub = Given

    /// Convenience method - call setupMock() to extend debug information when failure occurs
    public func setupMock(file: StaticString = #file, line: UInt = #line) {
        self.file = file
        self.line = line
    }

    /// Clear mock internals. You can specify what to reset (invocations aka verify, givens or performs) or leave it empty to clear all mock internals
    public func resetMock(_ scopes: MockScope...) {
        let scopes: [MockScope] = scopes.isEmpty ? [.invocation, .given, .perform] : scopes
        if scopes.contains(.invocation) { invocations = [] }
        if scopes.contains(.given) { methodReturnValues = [] }
        if scopes.contains(.perform) { methodPerformValues = [] }
    }





    open func handle(error: KiliaroError?) {
        addInvocation(.m_handle__error_error(Parameter<KiliaroError?>.value(`error`)))
		let perform = methodPerformValue(.m_handle__error_error(Parameter<KiliaroError?>.value(`error`))) as? (KiliaroError?) -> Void
		perform?(`error`)
    }

    open func handle(error: KiliaroError?, dismiss: (() -> Void)?) {
        addInvocation(.m_handle__error_errordismiss_dismiss(Parameter<KiliaroError?>.value(`error`), Parameter<(() -> Void)?>.value(`dismiss`)))
		let perform = methodPerformValue(.m_handle__error_errordismiss_dismiss(Parameter<KiliaroError?>.value(`error`), Parameter<(() -> Void)?>.value(`dismiss`))) as? (KiliaroError?, (() -> Void)?) -> Void
		perform?(`error`, `dismiss`)
    }


    fileprivate enum MethodType {
        case m_handle__error_error(Parameter<KiliaroError?>)
        case m_handle__error_errordismiss_dismiss(Parameter<KiliaroError?>, Parameter<(() -> Void)?>)

        static func compareParameters(lhs: MethodType, rhs: MethodType, matcher: Matcher) -> Matcher.ComparisonResult {
            switch (lhs, rhs) {
            case (.m_handle__error_error(let lhsError), .m_handle__error_error(let rhsError)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsError, rhs: rhsError, with: matcher), lhsError, rhsError, "error"))
				return Matcher.ComparisonResult(results)

            case (.m_handle__error_errordismiss_dismiss(let lhsError, let lhsDismiss), .m_handle__error_errordismiss_dismiss(let rhsError, let rhsDismiss)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsError, rhs: rhsError, with: matcher), lhsError, rhsError, "error"))
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsDismiss, rhs: rhsDismiss, with: matcher), lhsDismiss, rhsDismiss, "dismiss"))
				return Matcher.ComparisonResult(results)
            default: return .none
            }
        }

        func intValue() -> Int {
            switch self {
            case let .m_handle__error_error(p0): return p0.intValue
            case let .m_handle__error_errordismiss_dismiss(p0, p1): return p0.intValue + p1.intValue
            }
        }
        func assertionName() -> String {
            switch self {
            case .m_handle__error_error: return ".handle(error:)"
            case .m_handle__error_errordismiss_dismiss: return ".handle(error:dismiss:)"
            }
        }
    }

    open class Given: StubbedMethod {
        fileprivate var method: MethodType

        private init(method: MethodType, products: [StubProduct]) {
            self.method = method
            super.init(products)
        }


    }

    public struct Verify {
        fileprivate var method: MethodType

        public static func handle(error: Parameter<KiliaroError?>) -> Verify { return Verify(method: .m_handle__error_error(`error`))}
        public static func handle(error: Parameter<KiliaroError?>, dismiss: Parameter<(() -> Void)?>) -> Verify { return Verify(method: .m_handle__error_errordismiss_dismiss(`error`, `dismiss`))}
    }

    public struct Perform {
        fileprivate var method: MethodType
        var performs: Any

        public static func handle(error: Parameter<KiliaroError?>, perform: @escaping (KiliaroError?) -> Void) -> Perform {
            return Perform(method: .m_handle__error_error(`error`), performs: perform)
        }
        public static func handle(error: Parameter<KiliaroError?>, dismiss: Parameter<(() -> Void)?>, perform: @escaping (KiliaroError?, (() -> Void)?) -> Void) -> Perform {
            return Perform(method: .m_handle__error_errordismiss_dismiss(`error`, `dismiss`), performs: perform)
        }
    }

    public func given(_ method: Given) {
        methodReturnValues.append(method)
    }

    public func perform(_ method: Perform) {
        methodPerformValues.append(method)
        methodPerformValues.sort { $0.method.intValue() < $1.method.intValue() }
    }

    public func verify(_ method: Verify, count: Count = Count.moreOrEqual(to: 1), file: StaticString = #file, line: UInt = #line) {
        let fullMatches = matchingCalls(method, file: file, line: line)
        let success = count.matches(fullMatches)
        let assertionName = method.method.assertionName()
        let feedback: String = {
            guard !success else { return "" }
            return Utils.closestCallsMessage(
                for: self.invocations.map { invocation in
                    matcher.set(file: file, line: line)
                    defer { matcher.clearFileAndLine() }
                    return MethodType.compareParameters(lhs: invocation, rhs: method.method, matcher: matcher)
                },
                name: assertionName
            )
        }()
        MockyAssert(success, "Expected: \(count) invocations of `\(assertionName)`, but was: \(fullMatches).\(feedback)", file: file, line: line)
    }

    private func addInvocation(_ call: MethodType) {
        self.queue.sync { invocations.append(call) }
    }
    private func methodReturnValue(_ method: MethodType) throws -> StubProduct {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let candidates = sequencingPolicy.sorted(methodReturnValues, by: { $0.method.intValue() > $1.method.intValue() })
        let matched = candidates.first(where: { $0.isValid && MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch })
        guard let product = matched?.getProduct(policy: self.stubbingPolicy) else { throw MockError.notStubed }
        return product
    }
    private func methodPerformValue(_ method: MethodType) -> Any? {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let matched = methodPerformValues.reversed().first { MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch }
        return matched?.performs
    }
    private func matchingCalls(_ method: MethodType, file: StaticString?, line: UInt?) -> [MethodType] {
        matcher.set(file: file ?? self.file, line: line ?? self.line)
        defer { matcher.clearFileAndLine() }
        return invocations.filter { MethodType.compareParameters(lhs: $0, rhs: method, matcher: matcher).isFullMatch }
    }
    private func matchingCalls(_ method: Verify, file: StaticString?, line: UInt?) -> Int {
        return matchingCalls(method.method, file: file, line: line).count
    }
    private func givenGetterValue<T>(_ method: MethodType, _ message: String) -> T {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            onFatalFailure(message)
            Failure(message)
        }
    }
    private func optionalGivenGetterValue<T>(_ method: MethodType, _ message: String) -> T? {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            return nil
        }
    }
    private func onFatalFailure(_ message: String) {
        guard let file = self.file, let line = self.line else { return } // Let if fail if cannot handle gratefully
        SwiftyMockyTestObserver.handleFatalError(message: message, file: file, line: line)
    }
}

// MARK: - SettingsRouterProtocol

open class SettingsRouterProtocolMock: SettingsRouterProtocol, Mock {
    public init(sequencing sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst, stubbing stubbingPolicy: StubbingPolicy = .wrap, file: StaticString = #file, line: UInt = #line) {
        SwiftyMockyTestObserver.setup()
        self.sequencingPolicy = sequencingPolicy
        self.stubbingPolicy = stubbingPolicy
        self.file = file
        self.line = line
    }

    var matcher: Matcher = Matcher.default
    var stubbingPolicy: StubbingPolicy = .wrap
    var sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst

    private var queue = DispatchQueue(label: "com.swiftymocky.invocations", qos: .userInteractive)
    private var invocations: [MethodType] = []
    private var methodReturnValues: [Given] = []
    private var methodPerformValues: [Perform] = []
    private var file: StaticString?
    private var line: UInt?

    public typealias PropertyStub = Given
    public typealias MethodStub = Given
    public typealias SubscriptStub = Given

    /// Convenience method - call setupMock() to extend debug information when failure occurs
    public func setupMock(file: StaticString = #file, line: UInt = #line) {
        self.file = file
        self.line = line
    }

    /// Clear mock internals. You can specify what to reset (invocations aka verify, givens or performs) or leave it empty to clear all mock internals
    public func resetMock(_ scopes: MockScope...) {
        let scopes: [MockScope] = scopes.isEmpty ? [.invocation, .given, .perform] : scopes
        if scopes.contains(.invocation) { invocations = [] }
        if scopes.contains(.given) { methodReturnValues = [] }
        if scopes.contains(.perform) { methodPerformValues = [] }
    }





    open func dismiss() {
        addInvocation(.m_dismiss)
		let perform = methodPerformValue(.m_dismiss) as? () -> Void
		perform?()
    }


    fileprivate enum MethodType {
        case m_dismiss

        static func compareParameters(lhs: MethodType, rhs: MethodType, matcher: Matcher) -> Matcher.ComparisonResult {
            switch (lhs, rhs) {
            case (.m_dismiss, .m_dismiss): return .match
            }
        }

        func intValue() -> Int {
            switch self {
            case .m_dismiss: return 0
            }
        }
        func assertionName() -> String {
            switch self {
            case .m_dismiss: return ".dismiss()"
            }
        }
    }

    open class Given: StubbedMethod {
        fileprivate var method: MethodType

        private init(method: MethodType, products: [StubProduct]) {
            self.method = method
            super.init(products)
        }


    }

    public struct Verify {
        fileprivate var method: MethodType

        public static func dismiss() -> Verify { return Verify(method: .m_dismiss)}
    }

    public struct Perform {
        fileprivate var method: MethodType
        var performs: Any

        public static func dismiss(perform: @escaping () -> Void) -> Perform {
            return Perform(method: .m_dismiss, performs: perform)
        }
    }

    public func given(_ method: Given) {
        methodReturnValues.append(method)
    }

    public func perform(_ method: Perform) {
        methodPerformValues.append(method)
        methodPerformValues.sort { $0.method.intValue() < $1.method.intValue() }
    }

    public func verify(_ method: Verify, count: Count = Count.moreOrEqual(to: 1), file: StaticString = #file, line: UInt = #line) {
        let fullMatches = matchingCalls(method, file: file, line: line)
        let success = count.matches(fullMatches)
        let assertionName = method.method.assertionName()
        let feedback: String = {
            guard !success else { return "" }
            return Utils.closestCallsMessage(
                for: self.invocations.map { invocation in
                    matcher.set(file: file, line: line)
                    defer { matcher.clearFileAndLine() }
                    return MethodType.compareParameters(lhs: invocation, rhs: method.method, matcher: matcher)
                },
                name: assertionName
            )
        }()
        MockyAssert(success, "Expected: \(count) invocations of `\(assertionName)`, but was: \(fullMatches).\(feedback)", file: file, line: line)
    }

    private func addInvocation(_ call: MethodType) {
        self.queue.sync { invocations.append(call) }
    }
    private func methodReturnValue(_ method: MethodType) throws -> StubProduct {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let candidates = sequencingPolicy.sorted(methodReturnValues, by: { $0.method.intValue() > $1.method.intValue() })
        let matched = candidates.first(where: { $0.isValid && MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch })
        guard let product = matched?.getProduct(policy: self.stubbingPolicy) else { throw MockError.notStubed }
        return product
    }
    private func methodPerformValue(_ method: MethodType) -> Any? {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let matched = methodPerformValues.reversed().first { MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch }
        return matched?.performs
    }
    private func matchingCalls(_ method: MethodType, file: StaticString?, line: UInt?) -> [MethodType] {
        matcher.set(file: file ?? self.file, line: line ?? self.line)
        defer { matcher.clearFileAndLine() }
        return invocations.filter { MethodType.compareParameters(lhs: $0, rhs: method, matcher: matcher).isFullMatch }
    }
    private func matchingCalls(_ method: Verify, file: StaticString?, line: UInt?) -> Int {
        return matchingCalls(method.method, file: file, line: line).count
    }
    private func givenGetterValue<T>(_ method: MethodType, _ message: String) -> T {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            onFatalFailure(message)
            Failure(message)
        }
    }
    private func optionalGivenGetterValue<T>(_ method: MethodType, _ message: String) -> T? {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            return nil
        }
    }
    private func onFatalFailure(_ message: String) {
        guard let file = self.file, let line = self.line else { return } // Let if fail if cannot handle gratefully
        SwiftyMockyTestObserver.handleFatalError(message: message, file: file, line: line)
    }
}

// MARK: - SettingsViewControllerProtocol

open class SettingsViewControllerProtocolMock: SettingsViewControllerProtocol, Mock {
    public init(sequencing sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst, stubbing stubbingPolicy: StubbingPolicy = .wrap, file: StaticString = #file, line: UInt = #line) {
        SwiftyMockyTestObserver.setup()
        self.sequencingPolicy = sequencingPolicy
        self.stubbingPolicy = stubbingPolicy
        self.file = file
        self.line = line
    }

    var matcher: Matcher = Matcher.default
    var stubbingPolicy: StubbingPolicy = .wrap
    var sequencingPolicy: SequencingPolicy = .lastWrittenResolvedFirst

    private var queue = DispatchQueue(label: "com.swiftymocky.invocations", qos: .userInteractive)
    private var invocations: [MethodType] = []
    private var methodReturnValues: [Given] = []
    private var methodPerformValues: [Perform] = []
    private var file: StaticString?
    private var line: UInt?

    public typealias PropertyStub = Given
    public typealias MethodStub = Given
    public typealias SubscriptStub = Given

    /// Convenience method - call setupMock() to extend debug information when failure occurs
    public func setupMock(file: StaticString = #file, line: UInt = #line) {
        self.file = file
        self.line = line
    }

    /// Clear mock internals. You can specify what to reset (invocations aka verify, givens or performs) or leave it empty to clear all mock internals
    public func resetMock(_ scopes: MockScope...) {
        let scopes: [MockScope] = scopes.isEmpty ? [.invocation, .given, .perform] : scopes
        if scopes.contains(.invocation) { invocations = [] }
        if scopes.contains(.given) { methodReturnValues = [] }
        if scopes.contains(.perform) { methodPerformValues = [] }
    }

    public var viewController: KiliaroSceneViewController! {
		get {	invocations.append(.p_viewController_get); return __p_viewController ?? optionalGivenGetterValue(.p_viewController_get, "SettingsViewControllerProtocolMock - stub value for viewController was not defined") }
	}
	private var __p_viewController: (KiliaroSceneViewController)?





    open func showError(_ error: KiliaroError?) {
        addInvocation(.m_showError__error(Parameter<KiliaroError?>.value(`error`)))
		let perform = methodPerformValue(.m_showError__error(Parameter<KiliaroError?>.value(`error`))) as? (KiliaroError?) -> Void
		perform?(`error`)
    }


    fileprivate enum MethodType {
        case m_showError__error(Parameter<KiliaroError?>)
        case p_viewController_get

        static func compareParameters(lhs: MethodType, rhs: MethodType, matcher: Matcher) -> Matcher.ComparisonResult {
            switch (lhs, rhs) {
            case (.m_showError__error(let lhsError), .m_showError__error(let rhsError)):
				var results: [Matcher.ParameterComparisonResult] = []
				results.append(Matcher.ParameterComparisonResult(Parameter.compare(lhs: lhsError, rhs: rhsError, with: matcher), lhsError, rhsError, "_ error"))
				return Matcher.ComparisonResult(results)
            case (.p_viewController_get,.p_viewController_get): return Matcher.ComparisonResult.match
            default: return .none
            }
        }

        func intValue() -> Int {
            switch self {
            case let .m_showError__error(p0): return p0.intValue
            case .p_viewController_get: return 0
            }
        }
        func assertionName() -> String {
            switch self {
            case .m_showError__error: return ".showError(_:)"
            case .p_viewController_get: return "[get] .viewController"
            }
        }
    }

    open class Given: StubbedMethod {
        fileprivate var method: MethodType

        private init(method: MethodType, products: [StubProduct]) {
            self.method = method
            super.init(products)
        }

        public static func viewController(getter defaultValue: KiliaroSceneViewController?...) -> PropertyStub {
            return Given(method: .p_viewController_get, products: defaultValue.map({ StubProduct.return($0 as Any) }))
        }

    }

    public struct Verify {
        fileprivate var method: MethodType

        public static func showError(_ error: Parameter<KiliaroError?>) -> Verify { return Verify(method: .m_showError__error(`error`))}
        public static var viewController: Verify { return Verify(method: .p_viewController_get) }
    }

    public struct Perform {
        fileprivate var method: MethodType
        var performs: Any

        public static func showError(_ error: Parameter<KiliaroError?>, perform: @escaping (KiliaroError?) -> Void) -> Perform {
            return Perform(method: .m_showError__error(`error`), performs: perform)
        }
    }

    public func given(_ method: Given) {
        methodReturnValues.append(method)
    }

    public func perform(_ method: Perform) {
        methodPerformValues.append(method)
        methodPerformValues.sort { $0.method.intValue() < $1.method.intValue() }
    }

    public func verify(_ method: Verify, count: Count = Count.moreOrEqual(to: 1), file: StaticString = #file, line: UInt = #line) {
        let fullMatches = matchingCalls(method, file: file, line: line)
        let success = count.matches(fullMatches)
        let assertionName = method.method.assertionName()
        let feedback: String = {
            guard !success else { return "" }
            return Utils.closestCallsMessage(
                for: self.invocations.map { invocation in
                    matcher.set(file: file, line: line)
                    defer { matcher.clearFileAndLine() }
                    return MethodType.compareParameters(lhs: invocation, rhs: method.method, matcher: matcher)
                },
                name: assertionName
            )
        }()
        MockyAssert(success, "Expected: \(count) invocations of `\(assertionName)`, but was: \(fullMatches).\(feedback)", file: file, line: line)
    }

    private func addInvocation(_ call: MethodType) {
        self.queue.sync { invocations.append(call) }
    }
    private func methodReturnValue(_ method: MethodType) throws -> StubProduct {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let candidates = sequencingPolicy.sorted(methodReturnValues, by: { $0.method.intValue() > $1.method.intValue() })
        let matched = candidates.first(where: { $0.isValid && MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch })
        guard let product = matched?.getProduct(policy: self.stubbingPolicy) else { throw MockError.notStubed }
        return product
    }
    private func methodPerformValue(_ method: MethodType) -> Any? {
        matcher.set(file: self.file, line: self.line)
        defer { matcher.clearFileAndLine() }
        let matched = methodPerformValues.reversed().first { MethodType.compareParameters(lhs: $0.method, rhs: method, matcher: matcher).isFullMatch }
        return matched?.performs
    }
    private func matchingCalls(_ method: MethodType, file: StaticString?, line: UInt?) -> [MethodType] {
        matcher.set(file: file ?? self.file, line: line ?? self.line)
        defer { matcher.clearFileAndLine() }
        return invocations.filter { MethodType.compareParameters(lhs: $0, rhs: method, matcher: matcher).isFullMatch }
    }
    private func matchingCalls(_ method: Verify, file: StaticString?, line: UInt?) -> Int {
        return matchingCalls(method.method, file: file, line: line).count
    }
    private func givenGetterValue<T>(_ method: MethodType, _ message: String) -> T {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            onFatalFailure(message)
            Failure(message)
        }
    }
    private func optionalGivenGetterValue<T>(_ method: MethodType, _ message: String) -> T? {
        do {
            return try methodReturnValue(method).casted()
        } catch {
            return nil
        }
    }
    private func onFatalFailure(_ message: String) {
        guard let file = self.file, let line = self.line else { return } // Let if fail if cannot handle gratefully
        SwiftyMockyTestObserver.handleFatalError(message: message, file: file, line: line)
    }
}

